  typedef struct st{
    double* data;
    int length;

    int* shape;
    int dims;
    int owns_data;
  } St;

  int void_call();

//declarations
  int dec_int();
  int dec_double();
  int dec_float();
  int dec_array();
  int dec_struct();

//simple assignments
  int assign_var_int();
  int assign_var_char();
  int assign_var_float();
  int assign_var_double();
  int assign_var_var_int();
  int assign_var_var_char();
  int assign_var_var_float();
  int assign_var_var_double();
  int assign_var_var_addr();

//operations assignments
  int assign_add_int_int();
  int assign_add_float_float();
  int assign_add_double_double();
  int assign_add_var_var_int();
  int assign_add_var_var_float();
  int assign_add_var_var_double();
  int assign_add_var_int();
  int assign_add_var_float();
  int assign_add_var_double();

  int assign_sub_int_int();
  int assign_sub_float_float();
  int assign_sub_double_double();
  int assign_sub_var_var_int();
  int assign_sub_var_var_float();
  int assign_sub_var_var_double();
  int assign_sub_var_int();
  int assign_sub_var_float();
  int assign_sub_var_double();

  int assign_mul_int_int();
  int assign_mul_float_float();
  int assign_mul_double_double();
  int assign_mul_var_var_int();
  int assign_mul_var_var_float();
  int assign_mul_var_var_double();
  int assign_mul_var_int();
  int assign_mul_var_float();
  int assign_mul_var_double();

  int assign_div_int_int();
  int assign_div_float_float();
  int assign_div_double_double();
  int assign_div_var_var_int();
  int assign_div_var_var_float();
  int assign_div_var_var_double();
  int assign_div_var_int();
  int assign_div_var_float();
  int assign_div_var_double();

//Pointer Assignments
  int assign_var_ref_int();
  int assign_var_ref_char();
  int assign_var_ref_float();
  int assign_var_ref_double();
  int assign_var_ref_addr();

//Conditions
  int cmp_var_int();
  int cmp_var_char();
  int cmp_var_float();
  int cmp_var_double();
  int cmp_var_var_int();
  int cmp_var_var_char();
  int cmp_var_var_float();
  int cmp_var_var_double();

//Addresses and Casts
  int addr_calc();
  int arr_pos_load();
  int cast_int();
  int cast_char();
  int cast_float();
  int cast_double();
  int cast_addr();
