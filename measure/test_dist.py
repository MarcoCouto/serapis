#!/usr/bin/python

import json, re, sys, os, pickle
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

from decimal import *
from convolution import *
from lazyme.string import color_print
from multiprocessing import Pool

from dist_expr_evaluator import *

#devPad = Decimal(1.09702290514526E-09)
devPad = 2e-9 #Decimal(2e-9)
draw_result = True

shrinkInterval = 10  # from when to when should a convolution be shrunk
shrinkFactor = 2  # factor of convolution shrink (10 => size/10)
enableShrink = False
enableConvolving = True

max_count = sys.maxsize # 40000000

STEPS = 30

precision = 9  # round decimals precision

class Inst:
	
	def __init__(self, cpp, mean, measures, count, idx, action, method):
		super(Inst, self).__init__()
		self.action = action
		self.cpp = cpp
		self.mean = mean
		self.measures = measures # list(map(lambda i : i*1e9, measures))  # measures
		self.count = count
		self.idx = idx
		self.method = method

		if action == "multiply" :
			self.measures = list(map(lambda i: i*self.count, self.measures))

		self.grid = Grid(min(self.measures), max(self.measures), (max(self.measures) - min(self.measures))/STEPS)
		##self.dist = DistExprEvaluator.NormDist(self.mean, devPad, self.grid)
		#"""
		hist, bins = np.histogram(self.measures, bins=STEPS, density=True)
		bins = (bins[1:] + bins[:-1])*0.5  # get only the center of each bin
		#bins = np.delete(bins, len(bins)-1)
		self.dist = DistExprEvaluator.HistDist(self.idx, self.grid, None, hist, bins)
		#"""

	def multiply_measures(self, factor):
		measures = list(map(lambda i: i*factor, self.measures))
		#print(self.measures)

		grid = Grid(min(measures), max(measures), (max(measures) - min(measures))/STEPS)

		hist, bins = np.histogram(measures, bins=STEPS, density=True)
		bins = (bins[1:] + bins[:-1])*0.5  # get only the center of each bin
		return DistExprEvaluator.HistDist(self.idx, grid, None, hist, bins)

	def convolve(self):
		if self.count == 0:
			return 0

		color_print("Convolving " + self.cpp + "...", color="yellow")
		if self.action == "convolve":
			if self.method == "all-together":
				a = []
				for i in range(0,self.count):
					a.append(self.meaures)
				f = r_convolve(setA=a, setB=[], tag=self.cpp, decimals=precision, method=method)
			else:
				for i in range(0,self.count):
					#print("["+self.cpp+"] Going "+str(i))
					if i == 0:
						a = self.measures
					else:
						b = self.measures
						convolution_result = r_convolve(setA=a, setB=b, tag=self.cpp, decimals=precision, method=self.method)
						a = convolution_result
			color_print("Done with " + self.cpp, color="green")

		return 0

	def clean_convolve(self):
		#color_print("Convolving " + self.cpp + "...", color="yellow")
		res_dist = None
		if self.count == 0:
			return
		if self.action == "multiply":
			res_dist = self.dist
		else:
			#eval_grid = Grid(105, 120, 1e-6)
			
			dists = []
			"""
			count = self.count
			max_cut = max_count/50 # sys.maxsize
			while(count > 0):
				offset = min(count, max_cut)
				count -= offset
				dist_copy = self.multiply_measures(offset)
				dists.append(dist_copy)
			"""
			## < previous >	##
			for i in range(0,self.count):
				dists.append(self.dist)
			
			eval_grid = dists[0].grid
			#print("\t> Started working on", self.cpp)
			expr_evaluator = DistExprEvaluator(eval_grid) #, verbose=True)

			res_dist = expr_evaluator.convolve(dists)
			print("\t> Finished for", self.cpp)	
		with open("convolutions/"+self.cpp+str(self.idx)+"_dist.pkl", "wb+") as fp:
			pickle.dump(res_dist, fp, pickle.HIGHEST_PROTOCOL)

def generate_measures(eModel):
	for e in eModel:
		cpp = e['cpp']
		avg = Decimal(e['avg'])
		m = list(np.random.normal(avg, devPad, 100))
		measures = list(map(lambda x : abs(x), m))
		e['measures'] = measures

	return eModel

def save_model(eModel, model_file):
	with open(model_file, 'w+') as fp:
		json.dump(eModel, fp, indent=4, sort_keys=True)

def parse_path(content):
	content = content.replace("\"", "").replace(" ", "")
	prod_conf, path = "", ""
	aux = re.sub(r'\((.+), *\[(.+)\]\)', r'\1|\2', content).split("|")
	prod_conf = aux[0]
	all_instructions = aux[1].replace("]","").split(",(")
	path = []
	for x in all_instructions:
		y = x.replace(")","").replace("(","").split(",")
		path.append(y)

	return {prod_conf : path}

def get_model_entry(model, cpp):
	for e in model:
		if e['cpp'] == cpp:
			return e
	return None

def get_measures(model, cpp):
	model_entry = get_model_entry(model, cpp)
	if model_entry == None:
		return None
	else:
		return model_entry['measures']

def shrink_convolution(distribution, decimals, shrinkFactor):
	res = {}
	test = []
	if shrinkFactor > 0:
		count, keyCons, prob = 0, 0, 0
		for k, v in sorted(distribution.items()):
			count += 1
			if count < shrinkFactor:
				keyCons += k
				prob += v
			elif count == shrinkFactor:
				keyCons += k
				keyCons /= shrinkFactor
				res[round(keyCons, decimals)] = round(prob + v, decimals)
				test.append(prob + v)
				count, keyCons, prob = 0, 0, 0

		if count != 0:
			keyCons /= shrinkFactor
			test.append(prob)
			res[round(keyCons, decimals)] = prob
	else:
		res = distribution
	#print("testing... "+str(sum(res.values())))
	#print("[OK] testing... "+str(sum(test)))
	return res

def work(inst):
	#plt.hist(inst.measures, density=True)
	#plt.show()

	#inst.convolve()
	inst.clean_convolve()

def process(inst_list):
	# now, convolve 'count' distributions for every instruction first
	# taking advantage of multicore processing
	print(str(len(inst_list)) + " convolutions to perform")
	pool = Pool(processes=7)
	pool.map(work, inst_list)
	pool.close()
	pool.join()

def convolve_r(eModel, ePaths):
	multicore = True
	action = "convolve" # "multiply"       # 
	method = "all-together"   # "single"
	inst_list = []
	total_count = 0

	# first, clean previous results
	#"""
	check_output("rm -rf convolutions", shell=True)
	check_output("mkdir convolutions", shell=True)
	check_output("rm -rf convolve_results", shell=True)
	check_output("mkdir convolve_results", shell=True)
	#"""

	for e,p in enumerate(ePaths):
		#check_output("rm -rf convolutions/*", shell=True)
		# for every pair (product, path), expressed as a dictionary
		for key, val in p.items():
			# key = product configuration; val = list of instructions where the program passed
			i = 0
			print("Dividing effort...")
			for j, path in enumerate(val):
				# every instruction is represented as a pair: (cpp, #executions)
				cpp = path[0]
				count = int(path[1])
				total_count += count
				if count == 0:
					continue

				measures = get_measures(eModel, cpp)
				mean = float(get_model_entry(eModel, cpp)['avg'])
				#inst_list.append(Inst(cpp, mean, measures, count, 0, action, method))  #
				#"""
				idx = 0
				while (count > 0):
					i = min(max_count, count)
					inst_list.append(Inst(cpp, mean, measures, i, idx, action, method))
					count -= i
					idx += 1
				## break  ##
				#"""
			process(inst_list)
			color_print("Total Count: "+str(total_count), color="blue")

		color_print("Finishing "+str(e+1)+"...", color="green")
		if action == "convolve" or action == "multiply":  # means that the convolution of all intermediate convolutions is missing
			files = ["convolutions/"+f for f in os.listdir("convolutions") if os.path.isfile(os.path.join("convolutions", f)) and f.endswith(".pkl")]
			dists = list(map(lambda i : pickle.load(open(i, 'rb')), files))
			#dists = np.array(list(map(lambda i : pickle.load(open(i, 'rb')), files)))
			
			print(" >> creating the grid")
			eval_grid = dists[0].grid
			for d in dists:
				if d.grid.right > eval_grid.right:
					eval_grid = d.grid

			print(" >> convolving")
			expr_evaluator = DistExprEvaluator(eval_grid, verbose=True)
			res_dist = expr_evaluator.convolve(dists)
			color_print("|>>| "+str(res_dist.expectation()), color="blue")

			with open("convolve_results/prog_" + str(e) + "_dist.pkl", "wb+") as fp:
				pickle.dump(res_dist, fp, pickle.HIGHEST_PROTOCOL)
			#res_dist.print(0)
	
			#a = r_convolve(setA=[], setB=[], tag="final", decimals=precision, method=method)
			# TODO: Do something with 'a'
			return #continue
	
		if method == "single":
			a, b = [], []
			for i,inst in enumerate(inst_list):
				if i == 0:
					a = inst.measures
				else:
					b = inst.measures
					a = r_convolve(setA=a, setB=b, tag="all", decimals=precision, method=method)
				print(str(i) + " done")

		elif method == "all-together":
			setA = []
			for i, inst in enumerate(inst_list):
				setA.append(inst.measures)
			a = r_convolve(setA=setA, setB=[], tag="all", decimals=precision, method=method)
		break ## used only to stop after the first one
	color_print("ALL DONE!", color="green")

def convolve_python(eModel, ePaths):
	for p in ePaths:
		# for every pair (product, path), expressed as a dictionary
		for key, val in p.items():
			# key = product configuration; val = list of instructions where the program passed
			i = 0
			for j, path in enumerate(val):
				# every instruction is represented as a pair: (cpp, #executions)
				cpp = path[0]
				count = int(path[1])
##				i = i+1  ## s
				print("Convolving \'" + cpp + "\' [" + str(count) + "]")
				measures = get_measures(eModel, cpp)
				while (count > 0): # o
##				if count == 0:  ## s
##					print("ignoring...")  ## s
##					continue  ## s
##				if enableConvolving :  ## s
					i = i+1 # o
					count = count-1 # o
					if measures == None:
						color_print("[ERROR] There is no entry " + cpp + " in the energy model. Aborting...", color="red")
						exit(1)
					if i == 1:
						a = measures
						continue
					else:
						b = measures
						convolution_result = py_convolve(setA=a, setB=b, decimals=precision, draw=False, count=1)
						a = convolution_result
						s = len(a)
						
						if enableShrink and s > shrinkInterval:  # if i % shrinkInterval == 0:
							shrinkCount += 1
							#color_print("shrinking #" + str(shrinkCount), color="yellow")
							a = shrink_convolution(a, precision, shrinkFactor)
							#color_print(str(s) + " . . . " + str(len(a)), color="yellow")
							#print("[I] " + str(count) + " convolutions to go.")

		if draw_result:
			dist_space_result = np.linspace(min(list(convolution_result.keys())), max(list(convolution_result.keys())), len(convolution_result))
			plt.plot(dist_space_result, list(convolution_result.values()), color='green', label='convolution (A * B)')
			plt.legend(borderaxespad=0.)
			plt.show()
			print(str(convolution_result))

		# dist_space_result = np.linspace(min(list(convolution_result.keys())), max(list(convolution_result.keys())), len(convolution_result))
		# plot_convolution = plt.plot(dist_space_result, list(convolution_result.values()), color='green', label='convolution (A * B)')
		# plt.legend(borderaxespad=0.)

def main(model_file, path_file):
	eModel, ePaths = [], []
	plot_convolution = None
	shrinkCount = 0
	with open(model_file, 'r') as fp:
		eModel = json.load(fp)

	with open(path_file) as fp:
		content = fp.readlines()
	
	# Optional: generate measures
	#eModel = generate_measures(eModel)
	#save_model(eModel, model_file)

	for c in content:
		ePaths.append(parse_path(c))

	convolve_r(eModel, ePaths)

if __name__ == '__main__':
	model_file = "test.json"
	path_file = "clbg_paths.txt"
	main(model_file, path_file)
