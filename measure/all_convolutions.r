#!/usr/bin/env Rscript

library(distr)

args = commandArgs(trailingOnly=TRUE)


if (length(args) >= 1) {
	tag <- args[1]
	last.conv <- paste("convolutions/", tag, "_last_conv.rds", sep="")
	
	count <- 0
	if (length(args) == 2) {
		count <- as.numeric(args[2])
	}

	setX <- paste("convolutions/", tag, "_setX", sep="")
	X <- 0
	print("reading...")
	if (count == 0) {
		setX <- paste(setX, ".csv", sep="")
		x <- read.csv(setX, header = F)
	}else{
		x <- NULL
		j = 0
		while (j < count) {
			setX.file <- paste(setX, j, ".csv", sep="")
			x <- cbind(x, read.csv(setX.file, header = F)$V1)
		}
	}

	print("done")
	c <- 0
	for (i in x) {
		c <- c+1
		print(paste("Operation",c))
		if (c == 1){
			X <- Norm(mean=mean(i), sd=sd(i))
		}else{
			Y <- Norm(mean=mean(i), sd=sd(i))
			Z <- convpow(X+Y, 1)  # object of class AbscontDistribution
			X <- Z
		}
	}

	saveRDS(X, last.conv)

}else{
	print("Wrong number of arguments")
	exit(-1)
}