module Utils.Inlining 
(
    expandFunctions,
    removeFunctions,
    extractFunctions,
    getAllFunctions,
    getFunction,
    getStmtsFunction
) where

import Language.CIL.Types
import Language.CIL.StmtCore
import SPL.Feature
import Text.Regex (mkRegex, subRegex)
import Language.C.Data.Position (nopos)

expandFunctions :: Stmt -> Stmt
expandFunctions Null = Null
expandFunctions x = case x of Compound n stmts p -> Compound n (removeFunctions $ replaceApply (reverse $ tail $ reverse $ stmts) (extractFunctions x)) p
                              _ -> Null

replaceApply :: [Stmt] -> [Stmt] -> [Stmt]
replaceApply [] _ = []
replaceApply l [] = l
replaceApply (s:ss) funcs = case s of AssignApply _ _ _ -> expandApply s funcs ++ replaceApply ss funcs
                                      StmtApply a _ -> if not (isFeature a || isEndFeature a) 
                                                       then expandApply s funcs ++ replaceApply ss funcs
                                                       else s:replaceApply ss funcs
                                      --FunctionDef n t args (Compound ns sts p1) p -> FunctionDef n t args (Compound ns (replaceApply sts funcs) p1) p : replaceApply ss funcs
                                      FunctionDef "main" t args (Compound ns sts p1) p -> FunctionDef "main" t args (Compound ns (replaceApply sts funcs) p1) p : replaceApply ss funcs
                                      Compound ns sts p -> Compound ns (replaceApply sts funcs) p : replaceApply ss funcs
                                      While e sc p -> case sc of Compound n sts p1 -> While e (Compound n (replaceApply sts funcs) p1) p : replaceApply ss funcs
                                                                 _ -> While e (Compound [] (replaceApply [sc] funcs) p) p : replaceApply ss funcs
                                      If e sc1 sc2 p -> let scc1 = case sc1 of Compound n1 sts1 p1 -> Compound n1 (replaceApply sts1 funcs) p1
                                                                               _ -> Compound [] (replaceApply [sc1] funcs) p
                                                            scc2 = case sc2 of Compound n2 sts2 p2 -> Compound n2 (replaceApply sts2 funcs) p2
                                                                               _ -> Compound [] (replaceApply [sc2] funcs) p
                                                        in If e scc1 scc2 p : replaceApply ss funcs
                                      Switch e sc p -> case sc of Compound n sts p1 -> Switch e (Compound n (replaceApply sts funcs) p1) p : replaceApply ss funcs
                                                                  _ -> Switch e (Compound [] (replaceApply [sc] funcs) p) p : replaceApply ss funcs
                                      Case e sc p -> case sc of Compound n sts p1 -> Case e (Compound n (replaceApply sts funcs) p1) p : replaceApply ss funcs
                                                                _ -> let x = correctCaseStmt (s:ss)
                                                                     in replaceApply x funcs
                                      Default sc p -> case sc of Compound n sts p1 -> Default (Compound n (replaceApply sts funcs) p1) p : replaceApply ss funcs
                                                                 _ -> Default (Compound [] (replaceApply [sc] funcs) p) p : replaceApply ss funcs
                                      _ -> s:replaceApply ss funcs

expandApply :: Stmt -> [Stmt] -> [Stmt]
expandApply a [] = [a]
expandApply s@(StmtApply a@(Apply (Var n _) l) p) funcs | getFunction n funcs == [] = [s]
                                                        | otherwise = removeReturns (replaceApply (concat . map (getStmtsFunction l (clearName s)) $ (getFunction n funcs)) funcs) -- changeReturns "dummy_return"
expandApply s@(AssignApply exp (Apply (Var n _) l) p) funcs | getFunction n funcs == [] = [s]
                                                            | otherwise = changeReturns (assignVarName exp) (replaceApply (concat . map (getStmtsFunction l (clearName s)) $ (getFunction n funcs)) funcs)
-- {Deprecated} : never used
removeReturns :: [Stmt] -> [Stmt]
removeReturns [] = []
removeReturns (x:xs) = case x of Return _ _ -> removeReturns xs
                                 Compound ns sts p -> Compound ns (removeReturns sts) p:removeReturns xs
                                 If e (Compound n1 st1 p1) (Compound n2 st2 p2) p -> If e (Compound n1 (removeReturns st1) p1) (Compound n2 (removeReturns st2) p2) p:removeReturns xs
                                 While e (Compound ns st p2) p -> While e (Compound ns (removeReturns st) p2) p:removeReturns xs
                                 Switch e (Compound ns st p2) p -> Switch e (Compound ns (removeReturns st) p2) p:removeReturns xs
                                 Case e (Compound ns st p2) p -> Case e (Compound ns (removeReturns st) p2) p:removeReturns xs
                                 Default (Compound ns st p2) p -> Default (Compound ns (removeReturns st) p2) p:removeReturns xs
                                 _ -> x:removeReturns xs

changeReturns :: String -> [Stmt] -> [Stmt]
changeReturns var [] = []
changeReturns var (x:xs) = case x of Return (Just a) p -> AssignExpr (Var var p) a p: changeReturns var xs
                                     Return (Nothing) p -> changeReturns var xs
                                     Compound ns sts p -> Compound ns (changeReturns var sts) p:changeReturns var xs
                                     If e (Compound n1 st1 p1) (Compound n2 st2 p2) p -> If e (Compound n1 (changeReturns var st1) p1) (Compound n2 (changeReturns var st2) p2) p:changeReturns var xs
                                     While e (Compound ns st p2) p -> While e (Compound ns (changeReturns var st) p2) p:changeReturns var xs
                                     Switch e (Compound ns st p2) p -> Switch e (Compound ns (changeReturns var st) p2) p:changeReturns var xs
                                     Case e (Compound ns st p2) p -> Case e (Compound ns (changeReturns var st) p2) p:changeReturns var xs
                                     Default (Compound ns st p2) p -> Default (Compound ns (changeReturns var st) p2) p:changeReturns var xs
                                     _ -> x:changeReturns var xs

removeFunctions :: [Stmt] -> [Stmt]
removeFunctions [] = []
removeFunctions (x:xs) = case x of FunctionDef n _ _ _ _ -> if n /= "main" then removeFunctions xs else x:removeFunctions xs
                                   _ -> x:removeFunctions xs

-- FUNCTION FILTERS

extractFunctions :: Stmt -> [Stmt]
extractFunctions Null = []
extractFunctions x = case x of Compound _ stmts _ -> getAllFunctions stmts
                               _ -> []

getAllFunctions :: [Stmt] -> [Stmt]
getAllFunctions [] = []
getAllFunctions (x:xs) = case x of fd@(FunctionDef n _ _ _ _ )-> if n /= "main" then fd:getAllFunctions xs else getAllFunctions xs
                                   Compound _ st _ -> getAllFunctions xs ++ getAllFunctions st
                                   _ -> getAllFunctions xs

getFunction :: Name -> [Stmt] -> [Stmt]
getFunction _ [] = []
getFunction name list = let clName = subRegex (mkRegex "(.+)(__feature_.+)?") name "\\1"
                            x = filter (\(FunctionDef n _ _ _ _ ) -> n == clName) list
                        in x

getStmtsFunction :: [Expr] -> Stmt -> Stmt -> [Stmt]
getStmtsFunction expr call (FunctionDef n _ args (Compound _ l _) _) = let res | l == [] = [call]
                                                                               | otherwise = (map (swapVars (mergeLists (map (\(x,y)->x) args) expr)) . map (replaceVarsFunc n (getVarDeclar l))) l
                                                                           ini | isFeature' n = [(VariableDef ("__feature_"++featName' n) Void Nothing nopos)]
                                                                               | otherwise = []
                                                                           end | ini /= [] = [(VariableDef "__endfeature_" Void Nothing nopos)]
                                                                               | otherwise = []
                                                                       in (ini ++ res ++ end)


-- AUXILIAR FUNCTIONS

assignVarName :: Expr -> String
assignVarName (Var v _)      = v
assignVarName (Ind exp _)    = assignVarName exp
assignVarName (Index a b _)  = assignVarName a
assignVarName (Mem a b _)    = assignVarName a
assignVarName (MemInd a b _) = assignVarName a

clearName :: Stmt -> Stmt
clearName s@(StmtApply (Apply (Var n p0) l) p) = StmtApply (Apply ( Var (subRegex (mkRegex "(.+)(__feature_.+)?") n "\\1") p0) l) p
clearName s@(AssignApply (Var v p1) (Apply (Var n p2) l) p) = AssignApply (Var v p1) (Apply (Var (subRegex (mkRegex "(.+)(__feature_.+)?") n "\\1") p2) l) p

correctCaseStmt :: [Stmt] -> [Stmt]
correctCaseStmt [] = []
correctCaseStmt (s:ss) = case s of Case e st p -> Case e (Compound [] (injectInCase (st:ss)) p) p : correctCaseStmt ss
                                   Default st p -> Default (Compound [] (injectInCase (st:ss)) p) p : correctCaseStmt ss
                                   _ -> correctCaseStmt ss

injectInCase :: [Stmt] -> [Stmt]
injectInCase [] = []
injectInCase (s:ss) = case s of Case _ _ _ -> []
                                Default _ _ -> []
                                _ -> s : injectInCase ss

getVarDeclar :: [Stmt] -> [Name]
getVarDeclar [] = []
getVarDeclar (s:ss) = case s of VariableDef n _ _ _ -> n : getVarDeclar ss
                                _ -> getVarDeclar ss

replaceVarsFunc :: Name -> [Name] -> Stmt -> Stmt
replaceVarsFunc _ [] s = s
replaceVarsFunc fname vars s = case s of VariableDef n t i p -> if n `elem` vars then VariableDef (fname++"__"++n) t i p else s
                                         StmtApply (Apply e el) p -> StmtApply (Apply e (map (swapLocalVars fname vars) el)) p
                                         AssignApply e1 (Apply e2 el) p -> AssignApply (swapLocalVars fname vars e1) (Apply e2 (map (swapLocalVars fname vars) el)) p
                                         AssignExpr e1 e2 p -> AssignExpr (swapLocalVars fname vars e1) (swapLocalVars fname vars e2) p
                                         While e c p -> While (swapLocalVars fname vars e) (replaceVarsFunc fname vars c) p
                                         If e c1 c2 p -> If (swapLocalVars fname vars e) (replaceVarsFunc fname vars c1) (replaceVarsFunc fname vars c2) p
                                         Switch e c p -> Switch (swapLocalVars fname vars e) (replaceVarsFunc fname vars c) p
                                         Case e c p -> Case (swapLocalVars fname vars e) (replaceVarsFunc fname vars c) p
                                         Default c p -> Default (replaceVarsFunc fname vars c) p
                                         Compound ns sts p -> Compound ns (map (replaceVarsFunc fname vars) sts) p
                                         Return (Just e) p -> Return (Just (swapLocalVars fname vars e)) p
                                         _ -> s

swapLocalVars :: Name -> [Name] -> Expr -> Expr
swapLocalVars _ [] e = e
swapLocalVars fname l e = case e of Var    name      p -> if name `elem` l then Var (fname++"__"++name) p else e
                                    Mul    exp1 exp2 p -> Mul (swapLocalVars fname l exp1) (swapLocalVars fname l exp2) p
                                    Div    exp1 exp2 p -> Div (swapLocalVars fname l exp1) (swapLocalVars fname l exp2) p
                                    Rmd    exp1 exp2 p -> Rmd (swapLocalVars fname l exp1) (swapLocalVars fname l exp2) p
                                    Add    exp1 exp2 p -> Add (swapLocalVars fname l exp1) (swapLocalVars fname l exp2) p
                                    Sub    exp1 exp2 p -> Sub (swapLocalVars fname l exp1) (swapLocalVars fname l exp2) p
                                    Shl    exp1 exp2 p -> Shl (swapLocalVars fname l exp1) (swapLocalVars fname l exp2) p
                                    Shr    exp1 exp2 p -> Shr (swapLocalVars fname l exp1) (swapLocalVars fname l exp2) p
                                    Lt     exp1 exp2 p -> Lt (swapLocalVars fname l exp1) (swapLocalVars fname l exp2) p
                                    Gt     exp1 exp2 p -> Gt (swapLocalVars fname l exp1) (swapLocalVars fname l exp2) p
                                    Le     exp1 exp2 p -> Le (swapLocalVars fname l exp1) (swapLocalVars fname l exp2) p
                                    Ge     exp1 exp2 p -> Ge (swapLocalVars fname l exp1) (swapLocalVars fname l exp2) p
                                    Eq     exp1 exp2 p -> Eq (swapLocalVars fname l exp1) (swapLocalVars fname l exp2) p
                                    Neq    exp1 exp2 p -> Neq (swapLocalVars fname l exp1) (swapLocalVars fname l exp2) p
                                    And    exp1 exp2 p -> And (swapLocalVars fname l exp1) (swapLocalVars fname l exp2) p
                                    Xor    exp1 exp2 p -> Xor (swapLocalVars fname l exp1) (swapLocalVars fname l exp2) p
                                    Or     exp1 exp2 p -> Or (swapLocalVars fname l exp1) (swapLocalVars fname l exp1) p
                                    Adr    exp      p -> Adr (swapLocalVars fname l exp) p
                                    Ind    exp      p -> Ind (swapLocalVars fname l exp) p
                                    Minus  exp      p -> Minus (swapLocalVars fname l exp) p
                                    Comp   exp      p -> Comp (swapLocalVars fname l exp) p
                                    Neg    exp      p -> Neg (swapLocalVars fname l exp) p
                                    Cast   t exp p -> Cast t (swapLocalVars fname l exp) p
                                    Index  exp1 exp2 p -> Index (swapLocalVars fname l exp1) (swapLocalVars fname l exp2) p
                                    Mem    exp name p -> Mem (swapLocalVars fname l exp) name p
                                    MemInd exp name p -> MemInd (swapLocalVars fname l exp) name p
                                    SizeE  exp      p -> SizeE  (swapLocalVars fname l exp) p
                                    _ -> e

mergeLists :: [a] -> [b] -> [(a,b)]
mergeLists [] _ = []
mergeLists _ [] = []
mergeLists (a:as) (b:bs) = (a,b):mergeLists as bs

swapVars :: [(Name,Expr)] -> Stmt -> Stmt
swapVars [] s = s
swapVars tp s = case s of VariableDef n t (Just (Init e)) p -> VariableDef n t (Just (Init (swapIfExists tp e))) p
                          StmtApply (Apply e el) p -> StmtApply (Apply e (map (swapIfExists tp) el)) p
                          AssignApply e1 (Apply e2 el) p -> AssignApply (swapIfExists tp e1) (Apply (swapIfExists tp e2) (map (swapIfExists tp) el)) p
                          AssignExpr e1 e2 p -> AssignExpr (swapIfExists tp e1) (swapIfExists tp e2) p
                          While e c p -> While (swapIfExists tp e) (swapVars tp c) p
                          If e c1 c2 p -> If (swapIfExists tp e) (swapVars tp c1) (swapVars tp c2) p
                          Switch e c p -> Switch (swapIfExists tp e) (swapVars tp c) p
                          Case e c p -> Case (swapIfExists tp e) (swapVars tp c) p
                          Default c p -> Default (swapVars tp c) p
                          Compound ns sts p -> Compound ns (map (swapVars tp) sts) p
                          Return (Just e) p -> Return (Just (swapIfExists tp e)) p
                          _ -> s

swapIfExists :: [(Name,Expr)] -> Expr -> Expr
swapIfExists [] exp = exp
swapIfExists l@((n,e):xs) exp = case exp of Var    name      p -> if name == n then e else swapIfExists xs exp
                                            Mul    exp1 exp2 p -> Mul (swapIfExists l exp1) (swapIfExists l exp2) p
                                            Div    exp1 exp2 p -> Div (swapIfExists l exp1) (swapIfExists l exp2) p
                                            Rmd    exp1 exp2 p -> Rmd (swapIfExists l exp1) (swapIfExists l exp2) p
                                            Add    exp1 exp2 p -> Add (swapIfExists l exp1) (swapIfExists l exp2) p
                                            Sub    exp1 exp2 p -> Sub (swapIfExists l exp1) (swapIfExists l exp2) p
                                            Shl    exp1 exp2 p -> Shl (swapIfExists l exp1) (swapIfExists l exp2) p
                                            Shr    exp1 exp2 p -> Shr (swapIfExists l exp1) (swapIfExists l exp2) p
                                            Lt     exp1 exp2 p -> Lt (swapIfExists l exp1) (swapIfExists l exp2) p
                                            Gt     exp1 exp2 p -> Gt (swapIfExists l exp1) (swapIfExists l exp2) p
                                            Le     exp1 exp2 p -> Le (swapIfExists l exp1) (swapIfExists l exp2) p
                                            Ge     exp1 exp2 p -> Ge (swapIfExists l exp1) (swapIfExists l exp2) p
                                            Eq     exp1 exp2 p -> Eq (swapIfExists l exp1) (swapIfExists l exp2) p
                                            Neq    exp1 exp2 p -> Neq (swapIfExists l exp1) (swapIfExists l exp2) p
                                            And    exp1 exp2 p -> And (swapIfExists l exp1) (swapIfExists l exp2) p
                                            Xor    exp1 exp2 p -> Xor (swapIfExists l exp1) (swapIfExists l exp2) p
                                            Or     exp1 exp2 p -> Or (swapIfExists l exp1) (swapIfExists l exp1) p
                                            Adr    exp      p -> Adr (swapIfExists l exp) p
                                            Ind    exp      p -> Ind (swapIfExists l exp) p
                                            Minus  exp      p -> Minus (swapIfExists l exp) p
                                            Comp   exp      p -> Comp (swapIfExists l exp) p
                                            Neg    exp      p -> Neg (swapIfExists l exp) p
                                            Cast   t exp p -> Cast t (swapIfExists l exp) p
                                            Index  exp1 exp2 p -> Index (swapIfExists l exp1) (swapIfExists l exp2) p
                                            Mem    exp name p -> Mem (swapIfExists l exp) name p
                                            MemInd exp name p -> MemInd (swapIfExists l exp) name p
                                            SizeE  exp      p -> SizeE  (swapIfExists l exp) p
                                            _ -> exp
