module SPL.Feature 
(
    getFeatures,
    isFeature,
    isFeature',
    isEndFeature,
    isEndFeature',
    featNames, 
    featName',
    validConfigs, 
    chkConfig, 
    Configuration(..), 
    allConfigs, 
    nodeConfig,
    nodeConfig'
) where

import Data.String.Utils
import Language.CIL
import Language.CIL.Types
import Language.CIL.StmtCore
import Data.List
import Text.Regex (mkRegex, splitRegex, subRegex, matchRegex)

data Configuration = All
                   | Features [Name]
    deriving (Eq, Show)


-- FEATURE RELATED FUNCTIONS

-- get the list of all existing features
getFeatures :: Stmt -> [String]
getFeatures Null = []
getFeatures x = case x of Compound _ stmts _ -> nub . features $ stmts
                          _ -> []

features :: [Stmt] -> [String]
features [] = []
features (x:xs) = case x of AssignApply _ a _ -> if isFeature a then featNames a ++ features xs else features xs
                            StmtApply a _ -> if isFeature a then featNames a ++ features xs else features xs
                            VariableDef n _ _ _ -> if isFeature' n then featName' n : features xs else features xs
                            Compound _ st _ -> features xs ++ features st
                            FunctionDef _ _ _ st _ -> features (st:xs)
                            While _ st _ -> features (st:xs)
                            If _ st1 st2 _ -> features (st1:st2:xs)
                            Switch _ st _ -> features (st:xs)
                            Case _ st _ -> features (st:xs)
                            Default st _ -> features (st:xs)
                            _ -> features xs

isFeature :: Apply -> Bool
isFeature (Apply (Var x _) _) = startswith "__feature_" x
isFeature _ = False

isFeature' :: Name -> Bool
isFeature' n = startswith "__feature_" n || (matchRegex (mkRegex "(.+)__feature_(.+)") n /= Nothing)

featNames :: Apply -> [String]
featNames (Apply (Var x _) _) = splitRegex (mkRegex "&&|\\|\\|") . replace "not_" "" . replace "__feature_" "" $ x

featName' :: Name -> String
featName' n = subRegex (mkRegex "(.*)__feature_(.+)") n "\\2"

nodeConfig :: Apply -> [Name] -> Configuration
nodeConfig (Apply (Var x _) _) allF = Features(split "&&" . replace "__feature_" "" $ x)

nodeConfig' :: Name -> [Name] -> Configuration
nodeConfig' n allF = Features(split "&&" . replace "__feature_" "" $ n)

isEndFeature :: Apply -> Bool
isEndFeature (Apply (Var x _) _) = startswith "__endfeature_" x
isEndFeature _ = False

isEndFeature' :: Name -> Bool
isEndFeature' n = startswith "__endfeature_" n

validConfigs :: Stmt -> [Configuration]
validConfigs s = filter featureModel $ allConfigs . getFeatures $ s

allConfigs :: [Name] -> [Configuration]
allConfigs l = map (\x -> (Features x)) . subsequences $ l

-- A Feature Model with absolutely no restrictions

featureModel :: Configuration -> Bool
{--
featureModel All = True
featureModel (Features []) = True
featureModel (Features f) = ( ((not ("ENCRYPT" `elem` f)) || ("DECRYPT" `elem` f)) && ((not ("DECRYPT" `elem` f)) || ("ENCRYPT" `elem` f)) ) &&
                            ( ((not ("SIGN" `elem` f)) || ("VERIFY" `elem` f)) && ((not ("VERIFY" `elem` f)) || ("SIGN" `elem` f)) ) &&
                            ( (not ("ENCRYPT" `elem` f)) || ("KEYS" `elem` f) ) &&
                            ( (not ("SIGN" `elem` f)) || ("KEYS" `elem` f) )
--}
featureModel All = True
featureModel (Features []) = True
featureModel (Features f) = ((not ("OVERLOADED" `elem` f)) || ("WEIGHT" `elem` f)) &&
                            ((not ("TWO_THIRDS_FULL" `elem` f)) || ("WEIGHT" `elem` f))

--featureModel _ = True

-- Checks if the Configuration of the node matches a Product Configuration
-- Node Config -> Product Config
chkConfig :: Configuration -> Configuration -> Bool
chkConfig All _ = True
chkConfig (Features []) _ = True
chkConfig (Features (a:as)) (Features bs)
    | a `isFeatureOf` bs = chkConfig (Features as) (Features bs)
    | otherwise = False
chkConfig _ All = True

isFeatureOf :: Name -> [Name] -> Bool
isFeatureOf n [] = False
isFeatureOf n l | "||" `isInfixOf` n = (True `elem`) . (map (`isFeatureOf` l)) $ (split "||" n)
                | startswith "not_" n = not ((replace "not_" "" n) `isFeatureOf` l)
                | "><" `isInfixOf` n = not . (False `elem`) . (map (`isFeatureOf` l)) $ (split "><" n)
                | otherwise = n `elem` l