{-# LANGUAGE DeriveDataTypeable, StandaloneDeriving #-}

module Analysis.Models 
(
   getDelta, 
   arqModel, 
   deltaArqM, 
   joinL, 
   bbToVoc, 
   energyValue, 
   initState, 
   State(..), 
   EnergyModel
) where

import Language.CIL.Types
import Language.CIL.StmtCore
import Language.C.Data.Position
import Analysis.CFG
import Analysis.Types

import Data.Data
import Data.Generics.Uniplate.Data
import Data.List

import Language.HaLex.Dfa

deriving instance Data Expr
deriving instance Typeable Expr

deriving instance Data Type
deriving instance Typeable Type

data State = LowF | HighF
    deriving (Eq, Show)

type EnergyModel = [(String, Double)]  -- TODO: to consider more states, this type should be changed

initState = LowF

stateFromStr :: String -> State
stateFromStr "LowF" = LowF
stateFromStr "HighF" = HighF
stateFromStr _ = error "stateFromStr: invalid state"

data Voc = Nil | AddV | MulV | SubV | DivV
    deriving (Eq, Show)

vocModel = [Nil, AddV, SubV, MulV, DivV, AddV, SubV]
stateModel = [LowF, HighF]

arqModel = Dfa vocModel
               stateModel
               LowF
               stateModel
               deltaArqM

getDelta :: Dfa st sy -> (st -> sy -> st)
getDelta (Dfa _ _ _ _ d) = d

deltaArqM :: State -> Voc -> State
deltaArqM _ AddV = LowF
deltaArqM _ SubV = LowF
deltaArqM _ MulV = HighF
deltaArqM _ DivV = HighF
deltaArqM _ _ = LowF

joinL :: [State] -> State
joinL [] = LowF
joinL x | elem HighF x = HighF
        | otherwise = LowF

modelMap :: [(String, Double)]
modelMap = []

instructionEnergy :: String -> State -> EnergyModel -> (Double, [String])
instructionEnergy token s model = let f = filter (\(x,y) -> x == token) model
                                      d = if length f > 0
                                          then (snd . head $ f, [token])
                                          else error ("no energy value for "++token)
                                  in d

functionEnergy :: String -> [Expr]-> State -> EnergyModel -> (Double, [String])
functionEnergy name _ s model = let f = filter (\(x,y) -> x == "FUN_"++name) model
                                    d = if length f > 0
                                        then (snd . head $ f, ["FUN_"++name])
                                        else error ("no model entry for: " ++ name) --instructionEnergy "NULL" s model
                                in d

--Energy Consumption By Instruction
energyValue :: Stmt -> State -> EnergyModel -> [(Name, Type)] -> (Double, [String])
energyValue (VariableDef _ _ (Nothing) _) s model _ = (0, [])
energyValue (VariableDef _ Double (Just (Init _)) _) s model _ = instructionEnergy "ASS_VAR_DOUBLE" s model
energyValue (VariableDef _ Float (Just (Init _)) _) s model _ = instructionEnergy "ASS_VAR_FLOAT" s model
energyValue (VariableDef _ Int32 (Just (Init _)) _) s model _ = instructionEnergy "ASS_VAR_INT" s model
energyValue (VariableDef _ Int16 (Just (Init _)) _) s model _ = instructionEnergy "ASS_VAR_INT" s model
energyValue (VariableDef _ Int8 (Just (Init _)) _) s model _ = instructionEnergy "ASS_VAR_CHAR" s model
--energyValue (VariableDef _ (TypedefRef _) _ _) s model _ = instructionEnergy "DEC_STRUCT" s model --this depends on the contents of the struct
--energyValue (VariableDef _ (StructRef _) _ _) s model _ = instructionEnergy "DEC_STRUCT" s model --this depends on the contents of the struct
energyValue (VariableDef _ (Array _ _) _ _) s model _ = instructionEnergy "DEC_ARRAY" s model
energyValue (AssignExpr e1 (ConstChar _ _) _) s model vars = instructionEnergy "ASS_VAR_CHAR" s model
energyValue (AssignExpr e1 (ConstInt _ _) _) s model vars | getExpType vars e1 `elem` [Int32, Int16] = instructionEnergy "ASS_VAR_INT" s model
                                                          | getExpType vars e1 == Float = instructionEnergy "ASS_VAR_FLOAT" s model
                                                          | getExpType vars e1 == Double = instructionEnergy "ASS_VAR_DOUBLE" s model
                                                          | otherwise = instructionEnergy "ASS_VAR_DOUBLE" s model -- not totally correct. CHECK: why type analysis is not detecting the type of 'e1'
energyValue (AssignExpr e1 (ConstFloat _ _) _) s model vars | getExpType vars e1 == Float = instructionEnergy "ASS_VAR_FLOAT" s model
                                                            | getExpType vars e1 == Double = instructionEnergy "ASS_VAR_DOUBLE" s model
                                                            | otherwise = instructionEnergy "ASS_VAR_DOUBLE" s model -- not totally correct. CHECK: why type analysis is not detecting the type of 'e1'

--energyValue (AssignExpr (Ind src _) e _) s model vars = exprEnergy src s model vars + exprEnergy e s model vars
--energyValue (AssignExpr m@(MemInd (Var _ _) _ _) e _) s model vars = exprEnergy m s model vars + exprEnergy e s model vars
energyValue (AssignExpr src e _) s model vars = sumConcat [(exprEnergy src s model vars), (exprEnergy e s model vars)]

--missing other assignments!
energyValue (StmtApply (Apply (Var n _) args) _) s model vars = functionEnergy n args s model
energyValue (AssignApply _ (Apply (Var n _) args) _) s model vars = functionEnergy n args s model
energyValue (While e _ _) s model vars = exprEnergy e s model vars
energyValue (If e _ _ _) s model vars = exprEnergy e s model vars
energyValue (Case e _ _) s model vars = exprEnergy e s model vars
energyValue (Return (Just e) _) s model vars = exprEnergy e s model vars

--energyValue (TypeDecl _ _ _) _ _ _ = 0
--energyValue (Null) _ _ _ = 0
--energyValue (Break _) _ _ _ = 0
energyValue s _ _ _ = (0,[]) --error ("Missing energy case for statement: "++(show s))
--MISSING INSTRUCTIONS

--Energy Consumption By Arithmetic/Logic Operation
exprEnergy :: Expr -> State -> EnergyModel -> [(Name, Type)] -> (Double, [String])
exprEnergy v@(Var n _) s model vars | getExpType vars v `elem` [Int32, Int16] = instructionEnergy "ASS_VAR_VAR_INT" s model
                                    | getExpType vars v == Int8 = instructionEnergy "ASS_VAR_VAR_CHAR" s model
                                    | getExpType vars v == Float = instructionEnergy "ASS_VAR_VAR_FLOAT" s model
                                    | getExpType vars v == Double = instructionEnergy "ASS_VAR_VAR_DOUBLE" s model
                                    | otherwise = instructionEnergy "ASS_VAR_ADDR" s model

exprEnergy (Add (Var _ _) (ConstInt _ _) _) s model vars = instructionEnergy "ASS_ADD_VAR_INT" s model
exprEnergy (Add (Var _ _) (ConstFloat _ _) _) s model vars = instructionEnergy "ASS_ADD_VAR_DOUBLE" s model
exprEnergy (Add e (ConstInt _ _) _) s model vars = exprEnergy e s model vars -- + instructionEnergy "ASS_ADD_VAR_INT" s model
exprEnergy (Add e (ConstFloat _ _) _) s model vars = exprEnergy e s model vars -- + instructionEnergy "ASS_ADD_VAR_DOUBLE" s model
exprEnergy (Add (ConstInt _ _) (Var _ _)_) s model vars = instructionEnergy "ASS_ADD_VAR_INT" s model
exprEnergy (Add (ConstFloat _ _) (Var _ _)_) s model vars = instructionEnergy "ASS_ADD_VAR_DOUBLE" s model
exprEnergy (Add (ConstInt _ _) e _) s model vars = exprEnergy e s model vars -- + instructionEnergy "ASS_ADD_VAR_INT" s model
exprEnergy (Add (ConstFloat _ _) e _) s model vars = exprEnergy e s model vars -- + instructionEnergy "ASS_ADD_VAR_DOUBLE" s model
exprEnergy (Add e1@(Var _ _) e _) s model vars | getExpType vars e1 `elem`[Int32, Int16] = sumConcat [(exprEnergy e s model vars), (instructionEnergy "ASS_ADD_VAR_VAR_INT" s model)]
                                               | getExpType vars e1 == Float = sumConcat [(exprEnergy e s model vars), (instructionEnergy "ASS_ADD_VAR_VAR_FLOAT" s model)]
                                               | otherwise = sumConcat [(exprEnergy e s model vars), (instructionEnergy "ASS_ADD_VAR_VAR_DOUBLE" s model)]
                                               
exprEnergy (Add e e1@(Var _ _) _) s model vars | getExpType vars e1 `elem`[Int32, Int16] = sumConcat [(exprEnergy e s model vars), (instructionEnergy "ASS_ADD_VAR_VAR_INT" s model)]
                                               | getExpType vars e1 == Float = sumConcat [(exprEnergy e s model vars), (instructionEnergy "ASS_ADD_VAR_VAR_FLOAT" s model)]
                                               | otherwise = sumConcat [(exprEnergy e s model vars), (instructionEnergy "ASS_ADD_VAR_VAR_DOUBLE" s model)]
                                               
exprEnergy (Add e1 e2 _) s model vars | getExpType vars e1 `elem`[Int32, Int16] = sumConcat [(exprEnergy e1 s model vars), (exprEnergy e2 s model vars), (instructionEnergy "ASS_ADD_VAR_VAR_INT" s model)]
                                      | getExpType vars e1 == Float = sumConcat [(exprEnergy e1 s model vars), (exprEnergy e2 s model vars), (instructionEnergy "ASS_ADD_VAR_VAR_FLOAT" s model)]
                                      | otherwise = sumConcat [(exprEnergy e1 s model vars), (exprEnergy e2 s model vars), (instructionEnergy "ASS_ADD_VAR_VAR_DOUBLE" s model)]

exprEnergy (Sub (Var _ _) (ConstInt _ _) _) s model vars = instructionEnergy "ASS_SUB_VAR_INT" s model
exprEnergy (Sub (Var _ _) (ConstFloat _ _) _) s model vars = instructionEnergy "ASS_SUB_VAR_DOUBLE" s model
exprEnergy (Sub e (ConstInt _ _) _) s model vars = exprEnergy e s model vars -- + instructionEnergy "ASS_SUB_VAR_INT" s model
exprEnergy (Sub e (ConstFloat _ _) _) s model vars = exprEnergy e s model vars -- + instructionEnergy "ASS_SUB_VAR_DOUBLE" s model
exprEnergy (Sub (ConstInt _ _) (Var _ _)_) s model vars = instructionEnergy "ASS_SUB_VAR_INT" s model
exprEnergy (Sub (ConstFloat _ _) (Var _ _)_) s model vars = instructionEnergy "ASS_SUB_VAR_DOUBLE" s model
exprEnergy (Sub (ConstInt _ _) e _) s model vars = exprEnergy e s model vars -- + instructionEnergy "ASS_SUB_VAR_INT" s model
exprEnergy (Sub (ConstFloat _ _) e _) s model vars = exprEnergy e s model vars -- + instructionEnergy "ASS_SUB_VAR_DOUBLE" s model
exprEnergy (Sub e1@(Var _ _) e _) s model vars | getExpType vars e1 `elem`[Int32, Int16] = sumConcat [exprEnergy e s model vars, instructionEnergy "ASS_SUB_VAR_VAR_INT" s model]
                                               | getExpType vars e1 == Float = sumConcat [exprEnergy e s model vars, instructionEnergy "ASS_SUB_VAR_VAR_FLOAT" s model]
                                               | getExpType vars e1 == Double = sumConcat [exprEnergy e s model vars, instructionEnergy "ASS_SUB_VAR_VAR_DOUBLE" s model]
                                               | otherwise = sumConcat [exprEnergy e s model vars, instructionEnergy "ASS_SUB_VAR_VAR_DOUBLE" s model]  -- overestimation

exprEnergy (Sub e e1@(Var _ _) _) s model vars | getExpType vars e1 `elem`[Int32, Int16] = sumConcat [exprEnergy e s model vars, instructionEnergy "ASS_SUB_VAR_VAR_INT" s model]
                                               | getExpType vars e1 == Float = sumConcat [exprEnergy e s model vars, instructionEnergy "ASS_SUB_VAR_VAR_FLOAT" s model]
                                               | getExpType vars e1 == Double = sumConcat [exprEnergy e s model vars, instructionEnergy "ASS_SUB_VAR_VAR_DOUBLE" s model]
                                               | otherwise = sumConcat [exprEnergy e s model vars, instructionEnergy "ASS_SUB_VAR_VAR_DOUBLE" s model]  -- overestimation

exprEnergy (Sub e1 e2 _) s model vars | getExpType vars e1 `elem`[Int32, Int16] = sumConcat [exprEnergy e1 s model vars, exprEnergy e2 s model vars, instructionEnergy "ASS_SUB_VAR_VAR_INT" s model]
                                      | getExpType vars e1 == Float = sumConcat [exprEnergy e1 s model vars, exprEnergy e2 s model vars, instructionEnergy "ASS_SUB_VAR_VAR_FLOAT" s model]
                                      | otherwise = sumConcat [exprEnergy e1 s model vars, exprEnergy e2 s model vars, instructionEnergy "ASS_SUB_VAR_VAR_DOUBLE" s model]

exprEnergy (Mul (Var _ _) (ConstInt _ _) _) s model vars = instructionEnergy "ASS_MUL_VAR_INT" s model
exprEnergy (Mul (Var _ _) (ConstFloat _ _) _) s model vars = instructionEnergy "ASS_MUL_VAR_DOUBLE" s model
exprEnergy (Mul e (ConstInt _ _) _) s model vars = exprEnergy e s model vars -- + instructionEnergy "ASS_MUL_VAR_INT" s model
exprEnergy (Mul e (ConstFloat _ _) _) s model vars = exprEnergy e s model vars -- + instructionEnergy "ASS_MUL_VAR_DOUBLE" s model
exprEnergy (Mul (ConstInt _ _) (Var _ _)_) s model vars = instructionEnergy "ASS_MUL_VAR_INT" s model
exprEnergy (Mul (ConstFloat _ _) (Var _ _)_) s model vars = instructionEnergy "ASS_MUL_VAR_DOUBLE" s model
exprEnergy (Mul (ConstInt _ _) e _) s model vars = exprEnergy e s model vars -- + instructionEnergy "ASS_MUL_VAR_INT" s model
exprEnergy (Mul (ConstFloat _ _) e _) s model vars = exprEnergy e s model vars -- + instructionEnergy "ASS_MUL_VAR_DOUBLE" s model
exprEnergy (Mul e1@(Var _ _) e _) s model vars | getExpType vars e1 `elem`[Int32, Int16] = sumConcat [exprEnergy e s model vars, instructionEnergy "ASS_MUL_VAR_VAR_INT" s model]
                                               | getExpType vars e1 == Float = sumConcat [exprEnergy e s model vars, instructionEnergy "ASS_MUL_VAR_VAR_FLOAT" s model]
                                               | getExpType vars e1 == Double = sumConcat [exprEnergy e s model vars, instructionEnergy "ASS_MUL_VAR_VAR_DOUBLE" s model]
                                               | otherwise = sumConcat [exprEnergy e s model vars, instructionEnergy "ASS_MUL_VAR_VAR_DOUBLE" s model]  --overestimation

exprEnergy (Mul e e1@(Var _ _) _) s model vars | getExpType vars e1 `elem`[Int32, Int16] = sumConcat [exprEnergy e s model vars, instructionEnergy "ASS_MUL_VAR_VAR_INT" s model]
                                               | getExpType vars e1 == Float = sumConcat [exprEnergy e s model vars, instructionEnergy "ASS_MUL_VAR_VAR_FLOAT" s model]
                                               | getExpType vars e1 == Double = sumConcat [exprEnergy e s model vars, instructionEnergy "ASS_MUL_VAR_VAR_DOUBLE" s model]
                                               | otherwise = sumConcat [exprEnergy e s model vars, instructionEnergy "ASS_MUL_VAR_VAR_DOUBLE" s model]  --overestimation

exprEnergy (Mul e1 e2 _) s model vars | getExpType vars e1 `elem`[Int32, Int16] = sumConcat [exprEnergy e1 s model vars, exprEnergy e2 s model vars, instructionEnergy "ASS_MUL_VAR_VAR_INT" s model]
                                      | getExpType vars e1 == Float = sumConcat [exprEnergy e1 s model vars, exprEnergy e2 s model vars, instructionEnergy "ASS_MUL_VAR_VAR_FLOAT" s model]
                                      | otherwise = sumConcat [exprEnergy e1 s model vars, exprEnergy e2 s model vars, instructionEnergy "ASS_MUL_VAR_VAR_DOUBLE" s model]

exprEnergy (Div (Var _ _) (ConstInt _ _) _) s model vars = instructionEnergy "ASS_DIV_VAR_INT" s model
exprEnergy (Div (Var _ _) (ConstFloat _ _) _) s model vars = instructionEnergy "ASS_DIV_VAR_DOUBLE" s model
exprEnergy (Div e (ConstInt _ _) _) s model vars = exprEnergy e s model vars -- + instructionEnergy "ASS_DIV_VAR_INT" s model
exprEnergy (Div e (ConstFloat _ _) _) s model vars = exprEnergy e s model vars -- + instructionEnergy "ASS_DIV_VAR_DOUBLE" s model
exprEnergy (Div (ConstInt _ _) (Var _ _)_) s model vars = instructionEnergy "ASS_DIV_VAR_INT" s model
exprEnergy (Div (ConstFloat _ _) (Var _ _)_) s model vars = instructionEnergy "ASS_DIV_VAR_DOUBLE" s model
exprEnergy (Div (ConstInt _ _) e _) s model vars = exprEnergy e s model vars -- + instructionEnergy "ASS_DIV_VAR_INT" s model
exprEnergy (Div (ConstFloat _ _) e _) s model vars = exprEnergy e s model vars -- + instructionEnergy "ASS_DIV_VAR_DOUBLE" s model
exprEnergy (Div e1@(Var _ _) e _) s model vars | getExpType vars e1 `elem`[Int32, Int16] = sumConcat [exprEnergy e s model vars, instructionEnergy "ASS_DIV_VAR_VAR_INT" s model]
                                               | getExpType vars e1 == Float = sumConcat [exprEnergy e s model vars, instructionEnergy "ASS_DIV_VAR_VAR_FLOAT" s model]
                                               | getExpType vars e1 == Double = sumConcat [exprEnergy e s model vars, instructionEnergy "ASS_DIV_VAR_VAR_DOUBLE" s model]
                                               | otherwise = (0, [])

exprEnergy (Div e e1@(Var _ _) _) s model vars | getExpType vars e1 `elem`[Int32, Int16] = sumConcat [exprEnergy e s model vars, instructionEnergy "ASS_DIV_VAR_VAR_INT" s model]
                                               | getExpType vars e1 == Float = sumConcat [exprEnergy e s model vars, instructionEnergy "ASS_DIV_VAR_VAR_FLOAT" s model]
                                               | getExpType vars e1 == Double = sumConcat [exprEnergy e s model vars, instructionEnergy "ASS_DIV_VAR_VAR_DOUBLE" s model]
                                               | otherwise = (0, [])

exprEnergy (Div e1 e2 _) s model vars | getExpType vars e1 `elem`[Int32, Int16] = sumConcat [exprEnergy e1 s model vars, exprEnergy e2 s model vars, instructionEnergy "ASS_DIV_VAR_VAR_INT" s model]
                                      | getExpType vars e1 == Float = sumConcat [exprEnergy e1 s model vars, exprEnergy e2 s model vars, instructionEnergy "ASS_DIV_VAR_VAR_FLOAT" s model]
                                      | otherwise = sumConcat [exprEnergy e1 s model vars, exprEnergy e2 s model vars, instructionEnergy "ASS_DIV_VAR_VAR_DOUBLE" s model]

-- TODO: add instruction '%' to energy model (NAIVE as it is)
exprEnergy (Rmd e1 e2 p) s model vars = exprEnergy (Div e1 e2 p) s model vars
--

exprEnergy (Lt e1@(Var n _) e2@(Var m _) _) s model vars = cmplVarsEnergy [e1,e2] s model vars
exprEnergy (Lt ev@(Var n _) e _) s model vars = sumConcat [exprEnergy e s model vars, cmplEnergy ev s model vars]
exprEnergy (Lt e ev@(Var n _) _) s model vars = sumConcat [exprEnergy e s model vars, cmplEnergy ev s model vars]
exprEnergy n@(Lt e1 e2 _) s model vars = sumConcat [exprEnergy e1 s model vars, exprEnergy e2 s model vars, cmplEnergy n s model vars]

exprEnergy (Gt e1@(Var n _) e2@(Var m _) _) s model vars = cmplVarsEnergy [e1,e2] s model vars
exprEnergy (Gt ev@(Var n _) e _) s model vars = sumConcat [exprEnergy e s model vars, cmplEnergy ev s model vars]
exprEnergy (Gt e ev@(Var n _) _) s model vars = sumConcat [exprEnergy e s model vars, cmplEnergy ev s model vars]
exprEnergy n@(Gt e1 e2 _) s model vars = sumConcat [exprEnergy e1 s model vars, exprEnergy e2 s model vars, cmplEnergy n s model vars]

exprEnergy (Le e1@(Var n _) e2@(Var m _) _) s model vars = cmplVarsEnergy [e1,e2] s model vars
exprEnergy (Le ev@(Var n _) e _) s model vars = sumConcat [exprEnergy e s model vars, cmplEnergy ev s model vars]
exprEnergy (Le e ev@(Var n _) _) s model vars = sumConcat [exprEnergy e s model vars, cmplEnergy ev s model vars]
exprEnergy n@(Le e1 e2 _) s model vars = sumConcat [exprEnergy e1 s model vars, exprEnergy e2 s model vars, cmplEnergy n s model vars]

exprEnergy (Ge e1@(Var n _) e2@(Var m _) _) s model vars = cmplVarsEnergy [e1,e2] s model vars
exprEnergy (Ge ev@(Var n _) e _) s model vars = sumConcat [exprEnergy e s model vars, cmplEnergy ev s model vars]
exprEnergy (Ge e ev@(Var n _) _) s model vars = sumConcat [exprEnergy e s model vars, cmplEnergy ev s model vars]
exprEnergy n@(Ge e1 e2 _) s model vars = sumConcat [exprEnergy e1 s model vars, exprEnergy e2 s model vars, cmplEnergy n s model vars]

exprEnergy (Eq e1@(Var n _) e2@(Var m _) _) s model vars = cmplVarsEnergy [e1,e2] s model vars
exprEnergy (Eq ev@(Var n _) e _) s model vars = sumConcat [exprEnergy e s model vars, cmplEnergy ev s model vars]
exprEnergy (Eq e ev@(Var n _) _) s model vars = sumConcat [exprEnergy e s model vars, cmplEnergy ev s model vars]
exprEnergy n@(Eq e1 e2 _) s model vars = sumConcat [exprEnergy e1 s model vars, exprEnergy e2 s model vars, cmplEnergy n s model vars]

exprEnergy (Neq e1@(Var n _) e2@(Var m _) _) s model vars = cmplVarsEnergy [e1,e2] s model vars
exprEnergy (Neq ev@(Var n _) e _) s model vars = sumConcat [exprEnergy e s model vars, cmplEnergy ev s model vars]
exprEnergy (Neq e ev@(Var n _) _) s model vars = sumConcat [exprEnergy e s model vars, cmplEnergy ev s model vars]
exprEnergy n@(Neq e1 e2 _) s model vars = sumConcat [exprEnergy e1 s model vars, exprEnergy e2 s model vars, cmplEnergy n s model vars]

exprEnergy (And e1 e2 _) s model vars = sumConcat [exprEnergy e1 s model vars, exprEnergy e2 s model vars]
exprEnergy (Or e1 e2 _) s model vars = sumConcat [exprEnergy e1 s model vars, exprEnergy e2 s model vars]
exprEnergy (Xor e1 e2 _) s model vars = sumConcat [exprEnergy e1 s model vars, exprEnergy e2 s model vars]
exprEnergy (Neg e _) s model vars = exprEnergy e s model vars
exprEnergy (Minus e _) s model vars = exprEnergy e s model vars

exprEnergy (Ind (Ind e p1) p2) s model vars = exprEnergy (Ind e p1) s model vars
exprEnergy (Ind (Add e1 e2 _) _) s model vars = sumConcat [instructionEnergy "ADDR_CALC" s model, exprEnergy e1 s model vars, exprEnergy e2 s model vars]
exprEnergy (Ind e@(Var _ _) _) s model vars | getExpType vars e `elem` [Int32, Int16] = instructionEnergy "ASS_VAR_REF_INT" s model
                                            | getExpType vars e == Int8 = instructionEnergy "ASS_VAR_REF_CHAR" s model
                                            | getExpType vars e == Float = instructionEnergy "ASS_VAR_REF_FLOAT" s model
                                            | getExpType vars e == Double = instructionEnergy "ASS_VAR_REF_DOUBLE" s model
                                            | otherwise = instructionEnergy "ASS_VAR_REF_ADDR" s model
                                              
exprEnergy (Ind e _) s model vars = let val | getExpType vars e `elem` [Int32, Int16] = instructionEnergy "ASS_VAR_REF_INT" s model
                                            | getExpType vars e == Int8 = instructionEnergy "ASS_VAR_REF_CHAR" s model
                                            | getExpType vars e == Float = instructionEnergy "ASS_VAR_REF_FLOAT" s model
                                            | getExpType vars e == Double = instructionEnergy "ASS_VAR_REF_DOUBLE" s model
                                            | otherwise = instructionEnergy "ASS_VAR_REF_ADDR" s model
                                    in sumConcat [val, exprEnergy e s model vars]
exprEnergy (Adr e _) s model vars = case e of Var _ _ -> instructionEnergy "ASS_VAR_ADDR" s model
                                              _ -> sumConcat [instructionEnergy "ASS_VAR_ADDR" s model, exprEnergy e s model vars]

exprEnergy (Index e1 e2 _) s model vars = case e2 of Var _ _ ->sumConcat [instructionEnergy "ARR_POS_LOAD" s model, exprEnergy e1 s model vars]
                                                     _ -> sumConcat [instructionEnergy "ARR_POS_LOAD" s model, exprEnergy e1 s model vars, exprEnergy e2 s model vars]
                                                     
exprEnergy m@(Mem e n _) s model vars | getExpType vars e `elem` [Int32, Int16] = instructionEnergy "ASS_VAR_INT" s model
                                      | getExpType vars e == Int8 = instructionEnergy "ASS_VAR_CHAR" s model
                                      | getExpType vars e == Float = instructionEnergy "ASS_VAR_FLOAT" s model
                                      | otherwise = instructionEnergy "ASS_VAR_DOUBLE" s model

exprEnergy (MemInd (Ind (Var n p1) p2) tag p3) s model vars = exprEnergy (MemInd (Var n p2) tag p3) s model vars                                      
exprEnergy (MemInd e@(Var _ _) _ _) s model vars | getExpType vars e `elem` [Int32, Int16] = instructionEnergy "ASS_VAR_REF_INT" s model
                                                 | getExpType vars e == Int8 = instructionEnergy "ASS_VAR_REF_CHAR" s model
                                                 | getExpType vars e == Float = instructionEnergy "ASS_VAR_REF_FLOAT" s model
                                                 | getExpType vars e == Double = instructionEnergy "ASS_VAR_REF_DOUBLE" s model
                                                 | otherwise = instructionEnergy "ASS_VAR_REF_ADDR" s model

exprEnergy (MemInd e _ _) s model vars = let val | getExpType vars e `elem` [Int32, Int16] = instructionEnergy "ASS_VAR_REF_INT" s model
                                                 | getExpType vars e == Int8 = instructionEnergy "ASS_VAR_REF_CHAR" s model
                                                 | getExpType vars e == Float = instructionEnergy "ASS_VAR_REF_FLOAT" s model
                                                 | getExpType vars e == Double = instructionEnergy "ASS_VAR_REF_DOUBLE" s model
                                                 | otherwise = instructionEnergy "ASS_VAR_REF_ADDR" s model
                                         in sumConcat [val, exprEnergy e s model vars]

exprEnergy (Cast t e _) s model vars | t `elem`[Int32, Int16] = sumConcat [exprEnergy e s model vars, instructionEnergy "CAST_INT" s model]
                                     | t == Int8 = sumConcat [exprEnergy e s model vars, instructionEnergy "CAST_CHAR" s model]
                                     | t == Float = sumConcat [exprEnergy e s model vars, instructionEnergy "CAST_FLOAT" s model]
                                     | t == Double = sumConcat [exprEnergy e s model vars, instructionEnergy "CAST_DOUBLE" s model]
                                     | otherwise = sumConcat [exprEnergy e s model vars, instructionEnergy "CAST_ADDR" s model]

exprEnergy (Shl e1 e2 _) s model vars = sumConcat [exprEnergy e1 s model vars, exprEnergy e2 s model vars, instructionEnergy "SHL_INT" s model]
exprEnergy (Shr e1 e2 _) s model vars = sumConcat [exprEnergy e1 s model vars, exprEnergy e2 s model vars, instructionEnergy "SHR_INT" s model]
exprEnergy (SizeT _ _) s model vars = instructionEnergy "ASS_VAR_INT" s model  -- missing
exprEnergy (SizeE _ _) s model vars = instructionEnergy "ASS_VAR_INT" s model  -- missing

exprEnergy (ConstInt _ _) _ _ _ = (0, [])
exprEnergy (ConstChar _ _) _ _ _ = (0, [])
exprEnergy (ConstFloat _ _) _ _ _ = (0, [])
exprEnergy (ConstString _ _) _ _ _ = (0, [])
--MISSING SCENARIOS:
-- Comp (~a) <- what's this?

exprEnergy e _ _ _ = error ("Missing energy case for expression: "++(show e)) --0

cmplVarsEnergy :: [Expr] -> State -> EnergyModel -> [(Name, Type)] -> (Double, [String])
cmplVarsEnergy n s model vars = let t = map (getExpType vars) n
                                    r | Double `elem` t = instructionEnergy "CMP_VAR_VAR_DOUBLE" s model
                                      | Float `elem` t = instructionEnergy "CMP_VAR_VAR_FLOAT" s model
                                      | (Int32 `elem` t) || (Int16 `elem` t) = instructionEnergy "CMP_VAR_VAR_INT" s model
                                      | Int8 `elem` t = instructionEnergy "CMP_VAR_VAR_CHAR" s model
                                      | otherwise = instructionEnergy "CMP_VAR_VAR_INT" s model
                                in r

cmplEnergy :: Expr -> State -> EnergyModel -> [(Name, Type)]-> (Double, [String])
cmplEnergy n s model vars = let t = (getExpType vars) n
                                r | t `elem` [Int16, Int32] = instructionEnergy "CMP_VAR_INT" s model
                                  | t == Float = instructionEnergy "CMP_VAR_FLOAT" s model
                                  | t == Double = instructionEnergy "CMP_VAR_DOUBLE" s model
                                  | t == Int8 = instructionEnergy "CMP_VAR_CHAR" s model
                                  | otherwise = instructionEnergy "CMP_VAR_INT" s model
                            in r

sumConcat :: Num a => [(a, [b])] -> (a, [b])
sumConcat l = foldr (\(a,b) (c,d) -> (a+c, b++d)) (0,[]) l

-- AUX FUNCTIONS FOR MACHINE STATE MODEL --
bbToVoc :: BB -> Voc
bbToVoc (BB (s, _, c, _)) = case s of AssignExpr (Var n _) exp p -> getVoc exp
                                      _ -> Nil
bbToVoc _ = Nil

getVoc :: Expr -> Voc
getVoc expr = Nil
  {-
            let l = universe expr
                muls = [() | (Mul _ _ _) <- l]
                divs = [() | (Div _ _ _) <- l]
                adds = [() | (Add _ _ _) <- l]
                subs = [() | (Sub _ _ _) <- l]
            in if not (null muls)
               then MulV
               else if not (null divs)
                    then DivV
                    else if not (null adds)
                         then AddV
                         else if not (null subs) 
                              then SubV
                              else Nil
                              -}