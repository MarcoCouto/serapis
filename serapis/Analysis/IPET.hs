module Analysis.IPET
(
    ipet,
    genConstraints, 
    genLabels, 
    genOptimization,
    groupOpt,
    prevBounds
) where

import Numeric.LinearProgramming
import Data.Graph.Inductive
import Analysis.CFG
import Data.List
import Data.Ord
import Analysis.Flow

bound :: LNode BB -> [(Configuration, Double)] -- [EBound]
bound (i, (BB (s, d, c, e))) = map (\e' -> (fst e', fst . snd $ e')) e
bound (i, (Begin e)) = map (\e' -> (fst e', fst . snd $ e')) e
bound (i, (End e)) = map (\e' -> (fst e', fst . snd $ e')) e

-- Gen Labels
genLabels :: Gr BB Flow -> [String] 
genLabels gr = map tagN (labNodes gr) ++ map tagE (labEdges  gr)

tagN :: LNode a -> String
tagN (i, n) = "c" ++ show i

tagE :: LEdge b -> String
tagE (b, e, flow) = "c" ++ show b ++ "-" ++ show e

-- Gen Constraints
genConstraints :: Gr BB Flow -> MapM -> [String] -> Configuration -> Constraints
genConstraints gr m varTags config = let loopBounds = allLoopBounds gr [] m (loopNodes gr) config
                                     in General (concat . map (nodeConstraints gr loopBounds m varTags config) $ (labNodes gr))

nodeConstraints :: Gr BB Flow -> Visited -> MapM -> [String] -> Configuration -> LNode BB -> [Bound [(Double, Int)]]
nodeConstraints gr vis m tags config l@(n, bb) = let ind = 1#(indexOf tags (tagN l)+1)
                                                     inedges = (map tagE (inn gr n))
                                                     outedges = (map tagE (out gr n))
                                                     lBound | (filter(\(a,b,c) -> a == n) vis) == [] = -1
                                                            | otherwise = (\(f,s,t) -> s) . head . filter(\(a,b,c) -> a == n) $ vis
                                                     ret1 = if not(null inedges) 
                                                            then [(ind:(map (\x -> -1#x) (map ((+1) . indexOf tags) inedges))) :==: 0]
                                                            else if isStartNode bb 
                                                                 then [([ind] :==: 1)]
                                                                 else []
                                                     ret2 = if not(null outedges) 
                                                            then [(ind:(map (\x -> -1#x) (map ((+1) . indexOf tags) outedges))) :==: 0]
                                                            else if isEndNode bb 
                                                                 then [([ind] :==: 1)]
                                                                 else []
                                                     loop = if lBound /= -1 
                                                            then [([ind] :<=: (foldr (*) 1 (lBound:prevBounds gr l vis)))] -- <- when flow analysis is completed! lBound)] -- 
                                                            else []
                                                 in ret1 ++ ret2 ++ loop

prevBounds :: Gr BB Flow -> LNode BB -> Visited -> [Double]
prevBounds _ _ [] = []
prevBounds gr (nd,bb) vis = map (\(b,c,d) -> c-1) . filter (\(a,_,_) -> (isNested gr nd a)) $ vis
--prevBounds gr (nd,bb) vis = map (\(b,c,d) -> c-1) . filter (\(a,_,_) -> (a `elem` reachable nd gr) && (nd > a)) $ vis
--prevBounds _ _ _ = []

indexOf :: Eq a => [a] -> a -> Int
indexOf l x = case elemIndex x l of Just a -> a
                                    _ -> error "indexOf: elemIndex failed to find the index for an element "

-- Gen Optimization by Config
genOptimization :: Gr BB Flow -> [String] -> [(Configuration, Optimization)]
genOptimization gr varTags = map (\(x,y) -> (x, Maximize (y ++ edg))) . groupOpt . (map bound) $ (labNodes gr)
                             where edg = map (\a -> 0) (labEdges gr)


groupOpt :: [[(Configuration, Double)]] -> [(Configuration, [Double])]
groupOpt [] = []
groupOpt l = zip xs ys
             where xs = fst $ unzip $ head l
                   ys = transpose $ map (snd . unzip) l

ipet :: Gr BB Flow -> MapM -> [(Configuration, Solution)]
ipet g m = let labels = genLabels g
               opt = genOptimization g labels
           in map (\(c, o) -> (c, (exact o (genConstraints g m labels c) []))) opt
