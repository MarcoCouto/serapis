
module Analysis.Types
(
  getType, 
  getExpType, 
  allVars, 
  extractVarTypes,
  getAllTypeDecl
) where

import Language.CIL.Types
import Language.CIL.StmtCore

allVars :: [(Name, Type)]
allVars = []

getType :: [(Name, Type)] -> Name -> Type
getType l n = let f = filter (\(x,y) -> x == n) l
                  tp = if length f > 0
                      then (\(x,y) -> y) . head $ f
                      else Void
                  r = case tp of Array _ t -> Ptr t
                                 _ -> tp
              in r

typeByStr :: String -> Type
typeByStr "Void" = Void
typeByStr "Int8" = Int8
typeByStr "Int16" = Int16
typeByStr "Int32" = Int32
typeByStr "Word8" = Word8
typeByStr "Word16" = Word16
typeByStr "Word32" = Word32
typeByStr "Float" = Float
typeByStr "Double" = Double
{-
typeByStr "Array Int Type" = 
typeByStr "Ptr Type" = 
typeByStr "Volatile Type" = 
typeByStr "Typedef Type" = 
typeByStr "Struct [(Name, Type)]" = 
typeByStr "Union [(Name, Type)]" = 
typeByStr "Enum [(Name, Int)]" = 
typeByStr "BitField Type [(Name, Int)]" = 
typeByStr "StructRef Name" = 
typeByStr "UnionRef Name" = 
typeByStr "EnumRef Name" = 
typeByStr "TypedefRef Name" = 
typeByStr "Function Type [Type]" = 
-}
typeByStr _ = Void

getExpType :: [(Name, Type)] -> Expr -> Type
getExpType l (Var n _) = let x = getType l n
                             r = case x of Array _ (Array _ t) -> t
                                           Array _ t -> t
                                           Ptr t -> t
                                           _ -> x
                         in r 
getExpType l (ConstInt _ _) = Int32
getExpType l (ConstFloat _ _) = Double
getExpType l (ConstChar _ _) = Int8
getExpType l (ConstString s _) = Array (length s) Int8
getExpType l (Add e1 e2 _) = let a = getExpType l e1
                                 b = getExpType l e2
                                 c | a == Double || b == Double = Double
                                   | a == Float || b == Float = Float
                                   | otherwise = Int32
                           in c
getExpType l (Sub e1 e2 _) = let a = getExpType l e1
                                 b = getExpType l e2
                                 c | a == Double || b == Double = Double
                                   | a == Float || b == Float = Float
                                   | otherwise = Int32
                           in c
getExpType l (Mul e1 e2 _) = let a = getExpType l e1
                                 b = getExpType l e2
                                 c | a == Double || b == Double = Double
                                   | a == Float || b == Float = Float
                                   | otherwise = Int32
                             in c
getExpType l (Div e1 e2 _) = let a = getExpType l e1
                                 b = getExpType l e2
                                 c | a == Double || b == Double = Double
                                   | a == Float || b == Float = Float
                                   | otherwise = Int32
                             in c
getExpType l (Shl e1 e2 _) = let a = getExpType l e1
                                 b = getExpType l e2
                                 c | a == Double || b == Double = Double
                                   | a == Float || b == Float = Float
                                   | otherwise = Int32
                             in c
getExpType l (Shr e1 e2 _) = let a = getExpType l e1
                                 b = getExpType l e2
                                 c | a == Double || b == Double = Double
                                   | a == Float || b == Float = Float
                                   | otherwise = Int32
                             in c
getExpType l (Lt e1 e2 _) = let a = getExpType l e1
                                b = getExpType l e2
                                c | a == Double || b == Double = Double
                                  | a == Float || b == Float = Float
                                  | otherwise = Int32
                            in c
getExpType l (Gt e1 e2 _) = let a = getExpType l e1
                                b = getExpType l e2
                                c | a == Double || b == Double = Double
                                  | a == Float || b == Float = Float
                                  | otherwise = Int32
                            in c
getExpType l (Le e1 e2 _) = let a = getExpType l e1
                                b = getExpType l e2
                                c | a == Double || b == Double = Double
                                  | a == Float || b == Float = Float
                                  | otherwise = Int32
                            in c
getExpType l (Ge e1 e2 _) = let a = getExpType l e1
                                b = getExpType l e2
                                c | a == Double || b == Double = Double
                                  | a == Float || b == Float = Float
                                  | otherwise = Int32
                            in c
getExpType l (Eq e1 e2 _) = let a = getExpType l e1
                                b = getExpType l e2
                                c | a == Double || b == Double = Double
                                  | a == Float || b == Float = Float
                                  | otherwise = Int32
                            in c
getExpType l (Neq e1 e2 _) = let a = getExpType l e1
                                 b = getExpType l e2
                                 c | a == Double || b == Double = Double
                                   | a == Float || b == Float = Float
                                   | otherwise = Int32
                             in c
getExpType l (And e1 e2 _) = let a = getExpType l e1
                                 b = getExpType l e2
                                 c | a == Double || b == Double = Double
                                   | a == Float || b == Float = Float
                                   | otherwise = Int32
                             in c
getExpType l (Xor e1 e2 _) = let a = getExpType l e1
                                 b = getExpType l e2
                                 c | a == Double || b == Double = Double
                                   | a == Float || b == Float = Float
                                   | otherwise = Int32
                             in c
getExpType l (Or e1 e2 _) = let a = getExpType l e1
                                b = getExpType l e2
                                c | a == Double || b == Double = Double
                                  | a == Float || b == Float = Float
                                  | otherwise = Int32
                            in c
getExpType l (Cast t _ _) = t
getExpType l (Ind a _) = getExpType l a
getExpType l (Index a b _) = let tp = getExpType l a
                                 r = case tp of Array _ t -> t
                                                Ptr t -> t
                                                _ -> tp
                             in r
getExpType l (Mem (Var a _) n _) = let tp = getType l a
                                       r = case tp of Struct st -> snd . head . filter (\(tag, val) -> tag == n) $ st
                                                      _ -> Void
                                   in r
getExpType l (Mem (Index a p _) n _) = let tp = getExpType l a
                                           r = case tp of Struct st -> snd . head . filter (\(tag, val) -> tag == n) $ st
                                                          _ -> Void
                                       in r
getExpType l (MemInd (Var a _) n _) = let tp = getType l a
                                          r = case tp of Struct st -> snd . head . filter (\(tag,val) -> a == n) $ st
                                                         _ -> Void
                                      in r
getExpType l (MemInd (Ind (Var a p1) p2) tag p3) = getExpType l (MemInd (Var a p1) tag p2)

-- ...
getExpType _ _ = Int32    -- just in case...

getAllTypeDecl :: Stmt -> [(Name, Type)]
getAllTypeDecl (Compound _ sts _) = concat . map getAllTypeDecl $ sts
getAllTypeDecl (TypeDecl n t _) = [(n, t)]
getAllTypeDecl _ = []

fetchTypeByRef :: [(Name, Type)] -> Name -> Type
fetchTypeByRef l n = let fetch | (filter (\(a,b) -> a == n) $ l) == [] = EnumRef "first"
                               | otherwise = snd . head . filter (\(a,b) -> a == n) $ l
                         r = case fetch of StructRef ref -> let lll = filter (\(a,b) -> a == ref) $ l
                                                                xxx | lll == [] = EnumRef "second"
                                                                    | otherwise = snd . head $ lll
                                                            in xxx
                                           Typedef (StructRef ref) -> fetchTypeByRef l ref
                                           _ -> fetch
                     in r

extractVarTypes :: [(Name, Type)] -> Stmt -> [(Name, Type)]
extractVarTypes allT s = case s of VariableDef n (TypedefRef ref) _ _ -> [(n,fetchTypeByRef allT ref)]
                                   VariableDef n (StructRef ref) _ _ -> [(n,fetchTypeByRef allT ref)]
                                   VariableDef n (Ptr (TypedefRef ref)) _ _ -> [(n, Ptr (fetchTypeByRef allT ref))]
                                   VariableDef n (Ptr (StructRef ref)) _ _ -> [(n, Ptr (fetchTypeByRef allT ref))]
                                   VariableDef n (Array s (TypedefRef ref)) _ _ -> [(n, Array s (fetchTypeByRef allT ref))]
                                   VariableDef n (Array s (StructRef ref)) _ _ -> [(n, Array s (fetchTypeByRef allT ref))]
                                   VariableDef n (Array s (Array s2 (TypedefRef ref))) _ _ -> [(n, Array s (Array s2 (fetchTypeByRef allT ref)))]
                                   VariableDef n (Array s (Array s2 (StructRef ref))) _ _ -> [(n, Array s (Array s2 (fetchTypeByRef allT ref)))]
                                   VariableDef n t _ _ -> [(n,t)]
                                   FunctionDef _ _ _ s _ -> extractVarTypes allT s
                                   While _ s _ -> extractVarTypes allT s
                                   If _ s1 s2 _ -> extractVarTypes allT s1 ++ extractVarTypes allT s2
                                   Switch _ s _ -> extractVarTypes allT s
                                   Case _ s _ -> extractVarTypes allT s
                                   Default s _ -> extractVarTypes allT s
                                   Compound _ sl _ -> concat . map (extractVarTypes allT) $ sl
                                   _ -> []
