{-# LANGUAGE DeriveDataTypeable, StandaloneDeriving, DeriveGeneric, DeriveAnyClass #-}
module Main 
(
    module SPL.Feature,
    module Analysis.CFG,
    module Analysis.FixedPoint,
    module Analysis.Models,
    module Utils.Inlining,
    module Utils.UnParse,
    module Analysis.IPET,
    module Analysis.Flow,
    module Analysis.Types,
    main
) where

import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as BC
import qualified Data.ByteString.Lazy as BL
import qualified Data.Csv as CSV

import Language.CIL
import Language.CIL.StmtCore
import Data.Graph.Inductive
import Data.Graph.Inductive.Dot
import qualified Data.Vector as DV
import Numeric.LinearProgramming
import Data.List

import Utils.UnParse
import Utils.Inlining
import SPL.Feature
import Analysis.CFG
import Analysis.FixedPoint
import Analysis.Models
import Analysis.IPET
import Analysis.Flow
import Analysis.Types

import Control.DeepSeq

import Data.Function

deriving instance Show Constraints
deriving instance Show Optimization

main :: IO ()
main = do
         --let file = "/home/marco/repos/greens/phd_work/SPLs/MAT_FEUP/src/1-Original-NoZ3/_joined_/main_test.scc" --1
         --let file = "/home/marco/repos/greens/phd_work/SPLs/MAT_FEUP/src/2-Original-Z3/_joined_/main_test.scc" --2
         --let file = "/home/marco/repos/greens/phd_work/SPLs/MAT_FEUP/src/3-Unchecked/_joined_/main_test.scc" --3
         --let file = "/home/marco/repos/greens/phd_work/SPLs/MAT_FEUP/src/4-Modified1/_joined_/main_test.scc" --4
         --let file = "/home/marco/repos/greens/phd_work/SPLs/MAT_FEUP/src/5-Modified2/_joined_/main_test.scc" --5
         --let file = "/home/marco/repos/greens/phd_work/SPLs/MAT_FEUP/src/6-ModifiedC-NoUnnecessaryIntermediates/_joined_/main_test.scc" --6
         --let file = "/home/marco/repos/greens/phd_work/SPLs/MAT_FEUP/src/7-ModifiedC-ManualInterchange/_joined_/main_test.scc" --7
         --let file = "/home/marco/tests/trainings_SPLC/sum_of_multiples/_joined_/main.scc"
         let file = "/home/marco/tests/SPL_examples/elevator/_joined_/Main.scc"
         --let file = "/home/marco/tests/SPL_examples/elevator/_joined_/versions/Main_0.scc"
         --let file = "/home/marco/tests/SPL_examples/emailsystem/_joined_/Main.scc"
         --let file = "/home/marco/tests/SPL_examples/minepump/_joined_/Main.scc"
         --let file = "/home/marco/tests/exp1.cil"
         contents <- B.readFile file
         csvModel <- BL.readFile "data/energyModel.csv"
         -- input values for 'imleft' and 'imright'
         --imleft <- B.readFile "data/imleft.val"
         --imright <- B.readFile "data/imright.val"
         --let val1 = (read::String -> [Value])(BC.unpack imleft)
         --let val2 = (read::String -> [Value])(BC.unpack imright)
         --let mapM = [("pointedBy_imleft", val1),("pointedBy_imright", val2)]
         --let mapM = [("n", [ValI 10000])]
         let mapM        = []
         let energyModel = readCSV csvModel
         let ast         = parseCIL "spl.cil" contents
         let ast2        = expandFunctions ast
         let allVars     = getAllVars ast2
         let cfg         = testCFG ast2
         let appls       = fetchApplies cfg
         let confs       = testConfigs ast2
         let labs        = testLabels cfg
         let con         = testIPETConstraints cfg mapM
         let optim       = testIPETOptimization cfg confs energyModel allVars
         let nodeInfos   = genNodeInfos confs cfg
         let fixed       = fixedPoint cfg nodeInfos
         let cfgBounded  = localBounds cfg fixed energyModel allVars
         let opt         = genOptimization cfgBounded labs
         --print cfg
         --print (length appls)
         let dot = showDot (fglToDot cfg)
         writeFile "file.dot" dot
         --system("dot -Tpng -ofile.png file.dot")
         --print ast2
         --print "------"
         --print (length (labNodes cfg))
         --print "------"
         --print (length (labEdges cfg))
         --print allVars
         --print "Labels:"
         --print labs
         --print "Constraints:"
         --print "All Vars:"
         --print cfg
         --print "Solution:"
         --print optim
         --print con
         --print "------"
         --print nodeInfos
         --print fixed
         --print (checkCFG cfg)
         --print (checkCFG' cfg == [])
         --let nn5 = head . filter(\(x,y) -> x == 182) . labNodes $ cfg
         --let nn4 = head . filter(\(x,y) -> x == 117) . labNodes $ cfg
         --let nn3 = head . filter(\(x,y) -> x == 114) . labNodes $ cfg
         --let nn2 = head . filter(\(x,y) -> x == 92) . labNodes $ cfg
         --let nn1 = head . filter(\(x,y) -> x == 79) . labNodes $ cfg
         --let nn0 = head . filter(\(x,y) -> x == 63) . labNodes $ cfg
         --let lb = allLoopBounds cfg [] mapM (loopNodes cfg) All
         --print (map (\(a,b,c)->(a,b)) lb)
         --let vis = take 5 lb
         --let ev = eval cfg vis mapM (182,[]) All nn5
         --print (con)
         --print (optim)
         --print (confs)
         let solution = solve ast2 energyModel mapM allVars
         print (productSolution solution)
         print "--- --- ---"
         print (convolve solution cfgBounded [])
         --writeFile "data/spl_basic_unparsed.cpp" (show reParsed)
         --writeFile "data/spl_basic.ast2" (show ast2)
         --writeFile "data/spl_basic.ast" (show ast)

--testCFG :: Stmt -> Gr BB Flow
testCFG s = createCFG s

testConfigs s = validConfigs s

testLabels gr = genLabels gr

productSolution :: [(Configuration, Solution)] -> [(String, Double)]
productSolution [] = []
productSolution ((c, (Optimal (v,_))):t) = (show c, v):productSolution t
productSolution ((c, _):t) = (show c, -1):productSolution t

resV :: [(Configuration, Solution)] -> [Double]
resV [] = []
resV ((_, (Optimal (v,_))):t) = v:resV t
resV _ = []

resL :: [(Configuration, Solution)] -> [[Double]]
resL [] = []
resL ((_, (Optimal (_,l))):t) = l:resL t
resL _ = []

testIPETOptimization :: Gr BB Flow -> [Configuration] -> EnergyModel -> [(Name, Type)] -> [(Configuration, Optimization)]
testIPETOptimization gr vConf model vars = let nodeInfos = genNodeInfos vConf gr
                                               cfg = localBounds gr nodeInfos model vars
                                           in genOptimization cfg (genLabels cfg)


testIPETConstraints :: Gr BB Flow -> MapM -> Constraints
testIPETConstraints gr m = let l = genLabels gr
                               c = Features []
                           in genConstraints gr m l c


solve :: Stmt -> EnergyModel -> MapM -> [(Name, Type)] -> [(Configuration, Solution)]
solve s model mapM vars = let cfg = createCFG s                               -- create the CFG
                              vConf = validConfigs s                          -- generate all valid PC's
                              nodeInfos = genNodeInfos vConf cfg              -- generate per node static analysis info
                              fixed = fixedPoint cfg nodeInfos                -- fixed point computation
                              cfgBounded = localBounds cfg fixed model vars   -- calculate local energy bounds for each node
                              productsWCEC = ipet cfgBounded mapM             -- calculate the WCEC for all products
                          in productsWCEC

-- third argument, 'a', can be the updated energy model, with estimated consumption AND energy distributions stored
convolve :: [(Configuration, Solution)] -> Gr BB Flow -> a -> [(Configuration, [(String, Int)])]
convolve [] _ _ = []
convolve ((conf, sol):t) cfg x = case sol of
     Optimal(energy, lst) -> (conf, f cfg (take (noNodes cfg) lst)):convolve t cfg x
                       where f gr l = merge $ concatMap (\ (a1, b1) -> merge $ map (\ a2 -> (a2, round b1)) a1) $ flt $ zip (h $ labNodes gr) l

                             h nds = map (getEnergyTokens conf) nds
                             flt = filter (not . null . fst)

                             merge :: (Num a) => [(String, a)] -> [(String, a)]
                             merge l = map (\ l' -> (fst $ head l', sum $ map snd l')) $ groupBy ((==) `on` fst) $ sortOn fst l
     otherwise -> convolve t cfg x

getAllVars :: Stmt -> [(Name, Type)]
getAllVars s = extractVarTypes (getAllTypeDecl s) s

readCSV :: BL.ByteString -> [(String, Double)]
readCSV b = let d = CSV.decode CSV.NoHeader b :: Either String (DV.Vector (String, Double))
                r = case d of Right x -> DV.toList x
                              _ -> []
            in r

fetchDecls :: Gr BB Flow -> [LNode BB]
fetchDecls gr = filter (\(i, label) -> isDecl label) $ nodes
                where isDecl x = case x of BB ((VariableDef _ _ _ _), _, _, _) -> True
                                           _ -> False
                      nodes = labNodes gr

fetchApplies :: Gr BB Flow -> [Name]
fetchApplies gr = nub . map (\(a,b) -> getCallName b) . filter (\(i, label) -> isApply label) $ nodes
                  where isApply x = case x of BB ((StmtApply _ _), _, _, _) -> True
                                              BB ((AssignApply _ _ _), _, _, _) -> True
                                              _ -> False
                        getCallName y = case y of BB ((StmtApply (Apply (Var n _) _) _), _, _, _) -> n
                                                  BB ((AssignApply _ (Apply (Var n _) _) _), _, _, _) -> n
                                                  _ -> "---"
                        nodes = labNodes gr

-- outConvolution ::  -> [(, Int, a)]

-- para converter ficheiro {F}.c de C para CIL:
-- (1) myCPP {F}.c > {F}.mpp      <- transforma todos os #ifdefs em notações que a framework reconheça (TODO)
-- (2) cpp {F}.mpp > {F}.cpp      <- inclui no mesmo ficheiro as referências às funções usadas noutras libs
-- (3) cilly.native --noPrintLn {F}.cpp --out {F}.cil     <- transforma o código C em CIL
-- (4) myCPP -clean {F}.cil > {F}.cc                      <- limpa o código CIL (#line X, comments, ...)
