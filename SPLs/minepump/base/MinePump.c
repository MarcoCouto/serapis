#include <stdio.h>
#include "Environment.h"
#include "MinePump.h"


int pumpRunning = 0;
int systemActive = 1;

void timeShift() {
	if (pumpRunning)
		lowerWaterLevel();
	if (systemActive)
		processEnvironment();
}

void processEnvironment() {
	#ifdef HIGH_WATER_SENSOR
		int highWater = isHighWaterLevel();
		if (!pumpRunning && highWater) {
			activatePump();
			return;
		}
	#endif

	#ifdef LOW_WATER_SENSOR
		int lowWater = isLowWaterLevel();
		if (!pumpRunning && lowWater) {
			deactivatePump();
			return;
		}
	#endif

	#ifdef METHANE_ALARM
		int methaneAlarm = isMethaneAlarm();
		if (!pumpRunning && methaneAlarm) {
			deactivatePump();
			return;
		}
	#endif
}

void activatePump() {
	#ifdef METHANE_ALARM
		int methaneAlarm = isMethaneAlarm();
		if (methaneAlarm) {
			return;
		}
	#endif
	pumpRunning = 1;
}

void deactivatePump() {
	pumpRunning = 0;
}

int isMethaneAlarm() {
	return isMethaneLevelCritical();
}

int isPumpRunning() {
	return pumpRunning;
}

void printPump() {
	printf("Pump(System:");
	if (systemActive)
		printf("On");
	else
		printf("Off");
	printf(",Pump:");
	if (pumpRunning)
		printf("On");
	else
		printf("Off");
	printf(") ");
	printEnvironment();
	printf("\n");
}

// HIGH_WATER_SENSOR
int isHighWaterLevel() {
	return ! isHighWaterSensorDry();
}

// LOW_WATER_SENSOR
int isLowWaterLevel() {
	return ! isLowWaterSensorDry();
}

// START_COMMAND
void startSystem() {
	#ifdef START_COMMAND
		systemActive = 1;
	#endif
}

// STOP_SYSTEM
void stopSystem() {
	#ifdef STOP_SYSTEM
		if (pumpRunning) {
			deactivatePump();
		}
		systemActive = 0;
	#endif
}
