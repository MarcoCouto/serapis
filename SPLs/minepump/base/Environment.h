
void lowerWaterLevel() ;
void waterRise();
void changeMethaneLevel();
int isMethaneLevelCritical();
int getWaterLevel();

void printEnvironment();

// HIGH_WATER_SENSOR
int isHighWaterSensorDry();

// LOW_WATER_SENSOR
int isLowWaterSensorDry();