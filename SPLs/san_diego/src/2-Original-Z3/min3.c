/* Implementation file for min3 */

#include "MATISSE_raw_ind2sub.h"
#include "MATISSE_raw_sub2ind.h"
#include "lib/matisse.h"
#include "lib/matrix.h"
#include "lib/tensor.h"
#include "lib/tensor_struct.h"
#include <math.h>
#include "min3.h"
#include "new_from_matrix_double_0_d.h"
#include "new_from_matrix_double_1_d.h"
#include <stdlib.h>


/**
 */
void min3_tddi_undef_2(tensor_d* A, double B[1], int DIM, tensor_d** restrict y, tensor_d** restrict positions)
{
   tensor_i* A_size = NULL;
   int MATISSE_short_circuit_or_left;
   int MATISSE_short_circuit_or_out;
   int condition_1;
   tensor_i* zeros_arg1 = NULL;
   const char * zeros_arg2;
   int A_numel;
   tensor_d* A_2 = NULL;
   int iter;
   int y_numel;
   int A_numel_1;
   int iter_1;
   tensor_i* ones_arg1 = NULL;
   tensor_i* y_size = NULL;
   int size_in_dim;
   const char * zeros_arg2_1;
   int end;
   int i;
   tensor_d* pos = NULL;
   double value;
   int index;
   int j;
   tensor_d* test_pos = NULL;
   double value2;

   if((0) != 0){
      // error 'Second argument of min(A, B, C) must be an empty matrix.';
   }
   
   size_d(A, &A_size);
   MATISSE_short_circuit_or_left = DIM < 1;
   MATISSE_short_circuit_or_out = MATISSE_short_circuit_or_left || floor((double) DIM) != DIM;
   condition_1 = MATISSE_short_circuit_or_out || (1) != 1;
   if(condition_1){
      // error 'Dimension argument must be a positive integer scalar.';
   }
   
   if(DIM > A_size->length){
      //  Can't do y = A; due to limitations in type system (when A is int, class(A) is reported as int32)
      size_d(A, &zeros_arg1);
      zeros_arg2 = "double";
      new_from_matrix_double_0_d_ti_row_1(zeros_arg1, y);
      A_numel = A->length;
      new_array_d_2(A_numel, 1, &A_2);
      for(iter = 0; iter < A_numel; ++iter){
         A_2->data[iter] = A->data[iter];
      }
      
      y_numel = (*y)->length;
      A_numel_1 = A_2->length;
      if(A_numel_1 == 1){
      }else if(y_numel != A_numel_1){
         abort();
      }
      
      for(iter_1 = 0; iter_1 < y_numel; ++iter_1){
         (*y)->data[iter_1] = A_2->data[A_2->length == 1 ? 0 : iter_1];
      }
      
      size_d(A, &ones_arg1);
      new_from_matrix_double_1_d_ti_row_1(ones_arg1, positions);
   }else{
      if(A_size != NULL){
         new_array_helper_i(A_size->shape, A_size->dims, &y_size);
         copy_ti_pti(A_size, &y_size);
      }
      
      size_in_dim = y_size->data[DIM - 1];
      if(size_in_dim != 0){
         y_size->data[DIM - 1] = 1;
      }
      
      zeros_arg2_1 = "double";
      new_from_matrix_double_0_d_ti_row_1(y_size, y);
      new_from_matrix_double_0_d_ti_row_1(y_size, positions);
      end = (*y)->length;
      for(i = 1; i <= end; ++i){
         MATISSE_raw_ind2sub_tii_row_1(y_size, i, &pos);
         value = A->data[(int) (MATISSE_raw_sub2ind_titd_row_row_1(A_size, pos) - 1.0)];
         index = 1;
         for(j = 2; j <= size_in_dim; ++j){
            if(pos != NULL){
               new_array_helper_d(pos->shape, pos->dims, &test_pos);
               copy_td_ptd(pos, &test_pos);
            }
            
            test_pos->data[DIM - 1] = (double) j;
            value2 = A->data[(int) (MATISSE_raw_sub2ind_titd_row_row_1(A_size, test_pos) - 1.0)];
            if(value2 < value){
               value = value2;
               index = j;
            }
            
         }
         
         (*y)->data[(int) (MATISSE_raw_sub2ind_titd_row_row_1(y_size, pos) - 1.0)] = value;
         (*positions)->data[(int) (MATISSE_raw_sub2ind_titd_row_row_1(y_size, pos) - 1.0)] = (double) index;
      }
      
   }
   
   tensor_free_d(&A_2);
   tensor_free_i(&A_size);
   tensor_free_i(&ones_arg1);
   tensor_free_d(&pos);
   tensor_free_d(&test_pos);
   tensor_free_i(&y_size);
   tensor_free_i(&zeros_arg1);
}
