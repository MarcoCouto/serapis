/* Implementation file for MATISSE_raw_sub2ind */

#include "MATISSE_raw_sub2ind.h"
#include <inttypes.h>
#include "lib/tensor_struct.h"
#include <stdlib.h>


/**
 */
double MATISSE_raw_sub2ind_titd_row_row_1(tensor_i* sizes, tensor_d* indices)
{
   int num_indices;
   double output_1;
   double output_2;
   uint32_t dimAcc;
   int i;

   num_indices = sizes->length;
   output_1 = indices->data[0];
   //  Subtract one to correctly calculate the index
   output_2 = output_1 - 1.0;
   dimAcc = 1u;
   for(i = 2; i <= num_indices; ++i){
      dimAcc *= (uint32_t) sizes->data[i - 1 - 1];
      output_2 += (indices->data[i - 1] - 1.0) * (double) dimAcc;
   }
   
   //  Add one to adjust for MATLAB index
   
   return output_2 + 1.0;
}
