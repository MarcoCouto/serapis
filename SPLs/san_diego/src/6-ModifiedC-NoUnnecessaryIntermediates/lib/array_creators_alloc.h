/* Header file for lib/array_creators_alloc */

#ifndef LIB_ARRAY_CREATORS_ALLOC_H
#define LIB_ARRAY_CREATORS_ALLOC_H

#include "matisse.h"
#include <stdlib.h>
#include "tensor_struct.h"

/**
 */
tensor_c* zeros_c2(int dim_1, int dim_2, tensor_c** restrict t);

#endif
