/* Implementation file for getDisparity */

#include "getDisparity.h"
#include "lib/array_creators_alloc.h"
#include "lib/array_creators_dec.h"
#include "lib/general_matrix.h"
#include "lib/matisse.h"
#include "lib/matlab_general.h"
#include "lib/matrix.h"
#include "lib/tensor.h"
#include "lib/tensor_struct.h"
#include "min3.h"
#include "padarray_specialized_pre_post_tdd1x2_2.h"
#include "padarray_specialized_pre_tdi1x2s_2.h"
#include <stdlib.h>


/**
 */
void getDisparity_tdtdii_2d_2d_3(tensor_d* Ileft, tensor_d* Iright, int win_sz, int max_shift, tensor_d** restrict retDisparity, tensor_d** restrict retSAD, tensor_d** restrict minSAD)
{
   tensor_d* Ileft_1 = NULL;
   int numel_result;
   int iter_4;
   tensor_d* Iright_1 = NULL;
   int numel_result_1;
   int iter_3;
   int nr;
   int nc;
   int nb;
   double half_win_sz;
   double matrix_1[2];
   tensor_d* IleftPadded = NULL;
   double matrix[2];
   tensor_d* IrightPadded = NULL;
   int i;
   tensor_d* correlateSAD = NULL;
   int retSAD_end_1;
   int retSAD_end;
   int matrix_2[3];
   int iter_6;
   int iter_5;
   int retSAD_size1;
   int retSAD_size2;
   int retSAD_size3;
   int result_dim3;
   int iter_2;
   int iter_1;
   int iter;
   double retSAD_value;
   int is_less;
   tensor_d* IleftPadded_1 = NULL;
   tensor_d* IrightPadded_1 = NULL;
   double min_arg2[1];

   new_array_helper_d(Ileft->shape, Ileft->dims, &Ileft_1);
   numel_result = Ileft->length;
   for(iter_4 = 0; iter_4 < numel_result; ++iter_4){
      Ileft_1->data[iter_4] = (double) Ileft->data[iter_4];
   }
   
   new_array_helper_d(Iright->shape, Iright->dims, &Iright_1);
   numel_result_1 = Iright->length;
   for(iter_3 = 0; iter_3 < numel_result_1; ++iter_3){
      Iright_1->data[iter_3] = (double) Iright->data[iter_3];
   }
   
   size_multiargs_td_3_generic(Ileft_1, &nr, &nc, &nb);
   zeros_d3(nr, nc, max_shift, retSAD);
   if(win_sz > 1){
      half_win_sz = (double) win_sz / 2.0;
      // Removed no-op function call: matisse_new_array_from_dims
      matrix_1[0] = half_win_sz;
      matrix_1[1] = half_win_sz;
      padarray_specialized_pre_post_tdd1x2_2_tdd1x2_2d_1(Ileft_1, matrix_1, &IleftPadded);
      // Removed no-op function call: matisse_new_array_from_dims
      matrix[0] = half_win_sz;
      matrix[1] = half_win_sz;
      padarray_specialized_pre_post_tdd1x2_2_tdd1x2_2d_1(Iright_1, matrix, &IrightPadded);
      for(i = 0; i < max_shift; ++i){
         correlateSAD_tdtdii_undef_undef_1(IleftPadded, IrightPadded, win_sz, i, &correlateSAD);
         retSAD_end_1 = (*retSAD)->shape[0];
         retSAD_end = (*retSAD)->shape[1];
         // Removed no-op function call: matisse_new_array_from_dims
         matrix_2[0] = retSAD_end_1;
         matrix_2[1] = retSAD_end;
         matrix_2[2] = 1;
         for(iter_6 = 0; iter_6 < retSAD_end; ++iter_6){
            for(iter_5 = 0; iter_5 < retSAD_end_1; ++iter_5){
               (*retSAD)->data[(iter_5) + (iter_6) * (*retSAD)->shape[0] + (i) * (*retSAD)->shape[0]*(*retSAD)->shape[1]] = correlateSAD->data[correlateSAD->length == 1 ? 0 : iter_5 + iter_6 * (matrix_2[0]) + (1 - 1) * (matrix_2[0]) * (matrix_2[1])];
            }
            
         }
         
      }
      
      retSAD_size1 = (*retSAD)->shape[0];
      retSAD_size2 = (*retSAD)->shape[1];
      retSAD_size3 = (*retSAD)->dims < 3 ? 1 : (*retSAD)->shape[2];
      result_dim3 = min_scalars_dec_ii(1, retSAD_size3);
      new_array_d_3(retSAD_size1, retSAD_size2, result_dim3, minSAD);
      new_array_d_3(retSAD_size1, retSAD_size2, result_dim3, retDisparity);
      for(iter_2 = 1; iter_2 <= retSAD_size3; ++iter_2){
         for(iter_1 = 0; iter_1 < retSAD_size2; ++iter_1){
            for(iter = 0; iter < retSAD_size1; ++iter){
               retSAD_value = (*retSAD)->data[(iter) + (iter_1) * (*retSAD)->shape[0] + (iter_2 - 1) * (*retSAD)->shape[0]*(*retSAD)->shape[1]];
               is_less = iter_2 == 1 || retSAD_value < (*minSAD)->data[(iter) + (iter_1) * (*minSAD)->shape[0]];
               if(is_less){
                  (*retDisparity)->data[(iter) + (iter_1) * (*retDisparity)->shape[0]] = (double) iter_2;
                  (*minSAD)->data[(iter) + (iter_1) * (*minSAD)->shape[0]] = retSAD_value;
               }
               
            }
            
         }
         
      }
      
   }else{
      if(Ileft_1 != NULL){
         new_array_helper_d(Ileft_1->shape, Ileft_1->dims, &IleftPadded_1);
         copy_td_ptd(Ileft_1, &IleftPadded_1);
      }
      
      if(Iright_1 != NULL){
         new_array_helper_d(Iright_1->shape, Iright_1->dims, &IrightPadded_1);
         copy_td_ptd(Iright_1, &IrightPadded_1);
      }
      
      correlateSAD_tdtdii_2d_2d_1(IleftPadded_1, IrightPadded_1, win_sz, 0, retSAD);
      new_col_d(min_arg2);
      min3_tddi_undef_2(*retSAD, min_arg2, 3, minSAD, retDisparity);
   }
   
   tensor_free_d(&IleftPadded);
   tensor_free_d(&IleftPadded_1);
   tensor_free_d(&Ileft_1);
   tensor_free_d(&IrightPadded);
   tensor_free_d(&IrightPadded_1);
   tensor_free_d(&Iright_1);
   tensor_free_d(&correlateSAD);
}

/**
 */
tensor_d* correlateSAD_tdtdii_2d_2d_1(tensor_d* Ileft, tensor_d* Iright, int win_sz, int disparity, tensor_d** restrict retSAD)
{
   int matrix[2];
   tensor_d* Iright_moved_1 = NULL;
   int colon_arg2_6;
   int Iright_moved_end;
   int range_size;
   int ndims_result;
   int size;
   int iter_5;
   tensor_d* Iright_moved = NULL;
   int iter_4;
   int iter_3;
   int Iright_moved_index;
   int iter_2;
   int rows;
   int cols;
   tensor_d* SAD = NULL;
   int j;
   int i;
   double diff;
   tensor_d* integralImg = NULL;
   int colon_arg1_8;
   int colon_arg1_5;
   int range_size_1;
   int range_size_2;
   tensor_d* plus_arg1 = NULL;
   int colon_arg2_3;
   tensor_i* size_2 = NULL;
   tensor_d* minus_arg1_1 = NULL;
   int colon_arg1_1;
   tensor_i* size_3 = NULL;
   tensor_d* minus_arg1_7 = NULL;
   int colon_arg1_2;
   tensor_i* size_1 = NULL;
   int iter_1;
   int integralImg_index_4;
   int integralImg_index_1;
   int integralImg_index_3;
   int integralImg_index_6;
   int iter;

   // Removed no-op function call: matisse_new_array_from_dims
   matrix[0] = 0;
   matrix[1] = disparity;
   padarray_specialized_pre_tdi1x2s_2_tdi1x2_2d_1(Iright, matrix, &Iright_moved_1);
   colon_arg2_6 = Iright_moved_1->shape[1] - disparity;
   Iright_moved_end = Iright_moved_1->shape[0];
   range_size = colon_arg2_6 - 1 + 1;
   if(!(colon_arg2_6 <= Iright_moved_1->shape[1])){
      abort();
   }
   
   ndims_result = ndims_alloc_d(Iright_moved_1);
   size = 1;
   for(iter_5 = 3; iter_5 <= ndims_result; ++iter_5){
      size *= Iright_moved_1->dims < iter_5 ? 1 : Iright_moved_1->shape[iter_5 - 1];
   }
   
   new_array_d_3(Iright_moved_end, range_size, size, &Iright_moved);
   for(iter_4 = 0; iter_4 < size; ++iter_4){
      for(iter_3 = 1; iter_3 <= range_size; ++iter_3){
         Iright_moved_index = iter_3 + 1 - 1;
         for(iter_2 = 0; iter_2 < Iright_moved_end; ++iter_2){
            Iright_moved->data[(iter_2) + (iter_3 - 1) * Iright_moved->shape[0] + (iter_4) * Iright_moved->shape[0]*Iright_moved->shape[1]] = Iright_moved_1->data[(iter_2) + (Iright_moved_index - 1) * Iright_moved_1->shape[0] + (iter_4) * Iright_moved_1->shape[0]*Iright_moved_1->shape[1]];
         }
         
      }
      
   }
   
   size_multiargs_td_2_of_2(Ileft, &rows, &cols);
   new_array_d_2(rows, cols, &SAD);
   for(j = 0; j < cols; ++j){
      for(i = 0; i < rows; ++i){
         diff = Ileft->data[(i) + (j) * Ileft->shape[0]] - Iright_moved->data[(i) + (j) * Iright_moved->shape[0]];
         SAD->data[(i) + (j) * SAD->shape[0]] = diff * diff;
      }
      
   }
   
   // 2D scan.
   integralImage2D_td_2d_1(SAD, &integralImg);
   colon_arg1_8 = win_sz + 1;
   colon_arg1_5 = win_sz + 1;
   range_size_1 = integralImg->shape[0] - colon_arg1_8 + 1;
   range_size_2 = integralImg->shape[1] - colon_arg1_5 + 1;
   new_array_d_3(range_size_1, range_size_2, 1, &plus_arg1);
   colon_arg2_3 = integralImg->shape[1] - win_sz + 1;
   if(!(integralImg->shape[0] - win_sz + 1 <= integralImg->shape[0])){
      abort();
   }
   
   if(!(colon_arg2_3 <= integralImg->shape[1])){
      abort();
   }
   
   size_d(plus_arg1, &size_2);
   new_array_ti_d(size_2, &minus_arg1_1);
   colon_arg1_1 = win_sz + 1;
   if(!(integralImg->shape[0] - win_sz + 1 <= integralImg->shape[0])){
      abort();
   }
   
   size_d(minus_arg1_1, &size_3);
   new_array_ti_d(size_3, &minus_arg1_7);
   colon_arg1_2 = win_sz + 1;
   if(!(integralImg->shape[1] - win_sz + 1 <= integralImg->shape[1])){
      abort();
   }
   
   size_d(minus_arg1_7, &size_1);
   new_array_ti_d(size_1, retSAD);
   for(iter_1 = 1; iter_1 <= range_size_2; ++iter_1){
      integralImg_index_4 = iter_1 + colon_arg1_5 - 1;
      integralImg_index_1 = iter_1 + 2 - 1;
      integralImg_index_3 = iter_1 + colon_arg1_1 - 1;
      integralImg_index_6 = iter_1 + 2 - 1;
      for(iter = 1; iter <= range_size_1; ++iter){
         (*retSAD)->data[(iter - 1) + (iter_1 - 1) * (*retSAD)->shape[0]] = integralImg->data[(iter + colon_arg1_8 - 1 - 1) + (integralImg_index_4 - 1) * integralImg->shape[0]] + integralImg->data[(iter + 2 - 1 - 1) + (integralImg_index_1 - 1) * integralImg->shape[0]] - integralImg->data[(iter + 2 - 1 - 1) + (integralImg_index_3 - 1) * integralImg->shape[0]] - integralImg->data[(iter + colon_arg1_2 - 1 - 1) + (integralImg_index_6 - 1) * integralImg->shape[0]];
      }
      
   }
   
   tensor_free_d(&Iright_moved);
   tensor_free_d(&Iright_moved_1);
   tensor_free_d(&SAD);
   tensor_free_d(&integralImg);
   tensor_free_d(&minus_arg1_1);
   tensor_free_d(&minus_arg1_7);
   tensor_free_d(&plus_arg1);
   tensor_free_i(&size_1);
   tensor_free_i(&size_2);
   tensor_free_i(&size_3);
   
   return *retSAD;
}

/**
 */
tensor_d* integralImage2D_td_2d_1(tensor_d* I, tensor_d** restrict retImg)
{
   int nr;
   int nc;
   int nb;
   int I_end;
   int iter_2;
   int i;
   int retImg_arg1;
   int retImg_end;
   int iter_1;
   int j;
   int retImg_arg2;
   int retImg_end_1;
   int iter;

   size_multiargs_td_3_generic(I, &nr, &nc, &nb);
   zeros_d3(nr, nc, nb, retImg);
   if(!(1 <= I->shape[0])){
      abort();
   }
   
   I_end = I->shape[1];
   for(iter_2 = 0; iter_2 < I_end; ++iter_2){
      (*retImg)->data[iter_2] = I->data[iter_2];
   }
   
   for(i = 2; i <= nr; ++i){
      retImg_arg1 = i - 1;
      retImg_end = (*retImg)->shape[1];
      for(iter_1 = 0; iter_1 < retImg_end; ++iter_1){
         (*retImg)->data[(i - 1) + (iter_1) * (*retImg)->shape[0]] = (*retImg)->data[(retImg_arg1 - 1) + (iter_1) * (*retImg)->shape[0]] + I->data[(i - 1) + (iter_1) * I->shape[0]];
      }
      
   }
   
   // vtuneResumeMex;
   for(j = 2; j <= nc; ++j){
      retImg_arg2 = j - 1;
      retImg_end_1 = (*retImg)->shape[0];
      for(iter = 0; iter < retImg_end_1; ++iter){
         (*retImg)->data[(iter) + (j - 1) * (*retImg)->shape[0]] = (*retImg)->data[(iter) + (retImg_arg2 - 1) * (*retImg)->shape[0]] + (*retImg)->data[(iter) + (j - 1) * (*retImg)->shape[0]];
      }
      
   }
   
   // vtunePauseMex;
   
   return *retImg;
}

/**
 */
tensor_d* correlateSAD_tdtdii_undef_undef_1(tensor_d* Ileft, tensor_d* Iright, int win_sz, int disparity, tensor_d** restrict retSAD)
{
   int matrix[2];
   tensor_d* Iright_moved_1 = NULL;
   int colon_arg2_6;
   int Iright_moved_end;
   int range_size;
   int ndims_result;
   int size;
   int iter_5;
   tensor_d* Iright_moved = NULL;
   int iter_4;
   int iter_3;
   int Iright_moved_index;
   int iter_2;
   int rows;
   int cols;
   tensor_d* SAD = NULL;
   int j;
   int i;
   double diff;
   tensor_d* integralImg = NULL;
   int colon_arg1_8;
   int colon_arg1_5;
   int range_size_1;
   int range_size_2;
   tensor_d* plus_arg1 = NULL;
   int colon_arg2_3;
   tensor_i* size_2 = NULL;
   tensor_d* minus_arg1_1 = NULL;
   int colon_arg1_1;
   tensor_i* size_3 = NULL;
   tensor_d* minus_arg1_7 = NULL;
   int colon_arg1_2;
   tensor_i* size_1 = NULL;
   int iter_1;
   int integralImg_index_4;
   int integralImg_index_1;
   int integralImg_index_3;
   int integralImg_index_6;
   int iter;

   // Removed no-op function call: matisse_new_array_from_dims
   matrix[0] = 0;
   matrix[1] = disparity;
   padarray_specialized_pre_tdi1x2s_2_tdi1x2_undef_1(Iright, matrix, &Iright_moved_1);
   colon_arg2_6 = Iright_moved_1->shape[1] - disparity;
   Iright_moved_end = Iright_moved_1->shape[0];
   range_size = colon_arg2_6 - 1 + 1;
   if(!(colon_arg2_6 <= Iright_moved_1->shape[1])){
      abort();
   }
   
   ndims_result = ndims_alloc_d(Iright_moved_1);
   size = 1;
   for(iter_5 = 3; iter_5 <= ndims_result; ++iter_5){
      size *= Iright_moved_1->dims < iter_5 ? 1 : Iright_moved_1->shape[iter_5 - 1];
   }
   
   new_array_d_3(Iright_moved_end, range_size, size, &Iright_moved);
   for(iter_4 = 0; iter_4 < size; ++iter_4){
      for(iter_3 = 1; iter_3 <= range_size; ++iter_3){
         Iright_moved_index = iter_3 + 1 - 1;
         for(iter_2 = 0; iter_2 < Iright_moved_end; ++iter_2){
            Iright_moved->data[(iter_2) + (iter_3 - 1) * Iright_moved->shape[0] + (iter_4) * Iright_moved->shape[0]*Iright_moved->shape[1]] = Iright_moved_1->data[(iter_2) + (Iright_moved_index - 1) * Iright_moved_1->shape[0] + (iter_4) * Iright_moved_1->shape[0]*Iright_moved_1->shape[1]];
         }
         
      }
      
   }
   
   size_multiargs_td_2_generic(Ileft, &rows, &cols);
   new_array_d_2(rows, cols, &SAD);
   for(j = 0; j < cols; ++j){
      for(i = 0; i < rows; ++i){
         diff = Ileft->data[(i) + (j) * Ileft->shape[0]] - Iright_moved->data[(i) + (j) * Iright_moved->shape[0]];
         SAD->data[(i) + (j) * SAD->shape[0]] = diff * diff;
      }
      
   }
   
   // 2D scan.
   integralImage2D_td_2d_1(SAD, &integralImg);
   colon_arg1_8 = win_sz + 1;
   colon_arg1_5 = win_sz + 1;
   range_size_1 = integralImg->shape[0] - colon_arg1_8 + 1;
   range_size_2 = integralImg->shape[1] - colon_arg1_5 + 1;
   new_array_d_3(range_size_1, range_size_2, 1, &plus_arg1);
   colon_arg2_3 = integralImg->shape[1] - win_sz + 1;
   if(!(integralImg->shape[0] - win_sz + 1 <= integralImg->shape[0])){
      abort();
   }
   
   if(!(colon_arg2_3 <= integralImg->shape[1])){
      abort();
   }
   
   size_d(plus_arg1, &size_2);
   new_array_ti_d(size_2, &minus_arg1_1);
   colon_arg1_1 = win_sz + 1;
   if(!(integralImg->shape[0] - win_sz + 1 <= integralImg->shape[0])){
      abort();
   }
   
   size_d(minus_arg1_1, &size_3);
   new_array_ti_d(size_3, &minus_arg1_7);
   colon_arg1_2 = win_sz + 1;
   if(!(integralImg->shape[1] - win_sz + 1 <= integralImg->shape[1])){
      abort();
   }
   
   size_d(minus_arg1_7, &size_1);
   new_array_ti_d(size_1, retSAD);
   for(iter_1 = 1; iter_1 <= range_size_2; ++iter_1){
      integralImg_index_4 = iter_1 + colon_arg1_5 - 1;
      integralImg_index_1 = iter_1 + 2 - 1;
      integralImg_index_3 = iter_1 + colon_arg1_1 - 1;
      integralImg_index_6 = iter_1 + 2 - 1;
      for(iter = 1; iter <= range_size_1; ++iter){
         (*retSAD)->data[(iter - 1) + (iter_1 - 1) * (*retSAD)->shape[0]] = integralImg->data[(iter + colon_arg1_8 - 1 - 1) + (integralImg_index_4 - 1) * integralImg->shape[0]] + integralImg->data[(iter + 2 - 1 - 1) + (integralImg_index_1 - 1) * integralImg->shape[0]] - integralImg->data[(iter + 2 - 1 - 1) + (integralImg_index_3 - 1) * integralImg->shape[0]] - integralImg->data[(iter + colon_arg1_2 - 1 - 1) + (integralImg_index_6 - 1) * integralImg->shape[0]];
      }
      
   }
   
   tensor_free_d(&Iright_moved);
   tensor_free_d(&Iright_moved_1);
   tensor_free_d(&SAD);
   tensor_free_d(&integralImg);
   tensor_free_d(&minus_arg1_1);
   tensor_free_d(&minus_arg1_7);
   tensor_free_d(&plus_arg1);
   tensor_free_i(&size_1);
   tensor_free_i(&size_2);
   tensor_free_i(&size_3);
   
   return *retSAD;
}
