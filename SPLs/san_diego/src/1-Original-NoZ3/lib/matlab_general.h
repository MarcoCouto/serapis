/* Header file for lib/matlab_general */

/**#ifndef LIB_MATLAB_GENERAL_H**/
#define LIB_MATLAB_GENERAL_H

/**
 *  min implementation for Y = min(X,Z), when X and Z are both scalars 
 */
int min_scalars_dec_ii(int scalar1, int scalar2);

/**
 *  max implementation for Y = max(X,Z), when X and Z are both scalars 
 */
int max_scalars_dec_ii(int scalar1, int scalar2);

/**#endif**/
