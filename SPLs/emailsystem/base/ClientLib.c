#include <stdlib.h>
#include <string.h>
#include "ClientLib.h"

int __ste_Client_max     = 3;
int __ste_Client_counter = 0;

int initClient() {
	if (__ste_Client_counter < __ste_Client_max) {
		return ++__ste_Client_counter;
	} else {
		return -1;
	}
}

char __ste_client_names[3][50];
//char* __ste_client_name0 = 0;
//char* __ste_client_name1 = 0;
//char* __ste_client_name2 = 0;

char* getClientName(int handle) {
	char *res = NULL;
	if (handle >= 0 && handle < __ste_Client_max) {
		res = __ste_client_names[handle];
	}
	return res;
}
void setClientName(int handle, char* value) {
	if (handle >= 0 && handle < __ste_Client_max) {
		//__ste_client_names[handle] = value;
		strcpy(__ste_client_names[handle], value);
	}
}

int __ste_client_outbuffers[3];
//int __ste_client_outbuffer0 = 0;
//int __ste_client_outbuffer1 = 0;
//int __ste_client_outbuffer2 = 0;
//int __ste_client_outbuffer3 = 0;

int getClientOutbuffer(int handle) {
	int res = 0;
	if (handle >= 0 && handle < __ste_Client_max) {
		res = __ste_client_outbuffers[handle];
	}
	return res;
}
void setClientOutbuffer(int handle, int value) {
	if (handle >= 0 && handle < __ste_Client_max) {
		__ste_client_outbuffers[handle] = value;
	}
}

int __ste_ClientAddressBook_sizes[3] = {0, 0, 0};
//int __ste_ClientAddressBook_size0 = 0;
//int __ste_ClientAddressBook_size1 = 0;
//int __ste_ClientAddressBook_size2 = 0;

int getClientAddressBookSize(int handle){
	int res = 0;
	if (handle >= 0 && handle < __ste_Client_max) {
		res = __ste_ClientAddressBook_sizes[handle];
	}
	return res;
}

void setClientAddressBookSize(int handle, int value) {
	if (handle >= 0 && handle < __ste_Client_max) {
		__ste_ClientAddressBook_sizes[handle] = value;
	}
}

int createClientAddressBookEntry(int handle){
    int size = getClientAddressBookSize(handle);
	if (size < 3) {
	    setClientAddressBookSize(handle, size + 1);
		return size + 1;
	} else return -1;
}

int __ste_Client_AddressBook_Alias[3][3];
//int __ste_Client_AddressBook0_Alias0 = 0;
//int __ste_Client_AddressBook0_Alias1 = 0;
//int __ste_Client_AddressBook0_Alias2 = 0;
//int __ste_Client_AddressBook1_Alias0 = 0;
//int __ste_Client_AddressBook1_Alias1 = 0;
//int __ste_Client_AddressBook1_Alias2 = 0;
//int __ste_Client_AddressBook2_Alias0 = 0;
//int __ste_Client_AddressBook2_Alias1 = 0;
//int __ste_Client_AddressBook2_Alias2 = 0;

int getClientAddressBookAlias(int handle, int index) {
	int res = 0;
	if (handle < 0 || handle >= __ste_Client_max) {
		return 0;
	}

	if (index < 0 || index >= 3) {
		return 0;
	}

	res = __ste_Client_AddressBook_Alias[handle][index];
	return res;
}

int findClientAddressBookAlias(int handle, int userid) {
	int res = -1;
	if (handle < 0 || handle >= __ste_Client_max) {
		return -1;
	}

	int i;
	for (i = 0; i < 3; i++) {
		if (userid == __ste_Client_AddressBook_Alias[handle][i]) {
			res = i;
		}
	}

	return res;
}

void setClientAddressBookAlias(int handle, int index, int value) {
	if (handle >= 0 && handle < __ste_Client_max) {
		int *aux = __ste_Client_AddressBook_Alias[handle];
		if (index >= 0 && index < 3) {
			aux[index] = value;
		}
	}
}

/*
int __ste_Client_AddressBook0_Address0 = 0;
int __ste_Client_AddressBook0_Address1 = 0;
int __ste_Client_AddressBook0_Address2 = 0;
int __ste_Client_AddressBook1_Address0 = 0;
int __ste_Client_AddressBook1_Address1 = 0;
int __ste_Client_AddressBook1_Address2 = 0;
int __ste_Client_AddressBook2_Address0 = 0;
int __ste_Client_AddressBook2_Address1 = 0;
int __ste_Client_AddressBook2_Address2 = 0;

int getClientAddressBookAddress(int handle, int index) {
	if (handle == 1) {
		if (index == 0) {
			return __ste_Client_AddressBook0_Address0;
		} else if (index == 1) {
			return __ste_Client_AddressBook0_Address1;
		} else if (index == 2) {
			return __ste_Client_AddressBook0_Address2;
		} else {
			return 0;
		}
	} else if (handle == 2) {
		if (index == 0) {
			return __ste_Client_AddressBook1_Address0;
		} else if (index == 1) {
			return __ste_Client_AddressBook1_Address1;
		} else if (index == 2) {
			return __ste_Client_AddressBook1_Address2;
		} else {
			return 0;
		}
	} else if (handle == 3) {
		if (index == 0) {
			return __ste_Client_AddressBook2_Address0;
		} else if (index == 1) {
			return __ste_Client_AddressBook2_Address1;
		} else if (index == 2) {
			return __ste_Client_AddressBook2_Address2;
		} else {
			return 0;
		}
	} else {
		return 0;
	}
}
void setClientAddressBookAddress(int handle, int index, int value) {
	if (handle == 1) {
		if (index == 0) {
			__ste_Client_AddressBook0_Address0 = value;
		} else if (index == 1) {
			__ste_Client_AddressBook0_Address1 = value;
		} else if (index == 2) {
			__ste_Client_AddressBook0_Address2 = value;
		}
	} else if (handle == 2) {
		if (index == 0) {
			__ste_Client_AddressBook1_Address0 = value;
		} else if (index == 1) {
			__ste_Client_AddressBook1_Address1 = value;
		} else if (index == 2) {
			__ste_Client_AddressBook1_Address2 = value;
		}
	} else if (handle == 3) {
		if (index == 0) {
			__ste_Client_AddressBook2_Address0 = value;
		} else if (index == 1) {
			__ste_Client_AddressBook2_Address1 = value;
		} else if (index == 2) {
			__ste_Client_AddressBook2_Address2 = value;
		}
	} 
}
*/
int __ste_client_autoResponses[3];
//int __ste_client_autoResponse0 = 0;
//int __ste_client_autoResponse1 = 0;
//int __ste_client_autoResponse2 = 0;

int getClientAutoResponse(int handle) {
	int res = -1;
	if (handle >= 0 && handle < __ste_Client_max) {
		res = __ste_client_autoResponses[handle];
	}
	return res;
}

void setClientAutoResponse(int handle, int value) {
	if (handle >= 0 && handle < __ste_Client_max) {
		__ste_client_autoResponses[handle] = value;
	} 
}
int __ste_client_privateKeys[3];
//int __ste_client_privateKey0 = 0;
//int __ste_client_privateKey1 = 0;
//int __ste_client_privateKey2 = 0;

int getClientPrivateKey(int handle) {
	int res = 0;
	if (handle >= 0 && handle < __ste_Client_max) {
		res = __ste_client_privateKeys[handle];
	}
	return res;
}
void setClientPrivateKey(int handle, int value) {
	if (handle >= 0 && handle < __ste_Client_max) {
		__ste_client_privateKeys[handle] = value;
	}
}
int __ste_ClientKeyring_sizes[3];
//int __ste_ClientKeyring_size0 = 0;
//int __ste_ClientKeyring_size1 = 0;
//int __ste_ClientKeyring_size2 = 0;

int getClientKeyringSize(int handle){
	int res = 0;
	if (handle >= 0 && handle < __ste_Client_max) {
		res = __ste_ClientKeyring_sizes[handle];
	}
	return res;
}
void setClientKeyringSize(int handle, int value) {
	if (handle >= 0 && handle < __ste_Client_max) {
		__ste_ClientKeyring_sizes[handle] = value;
	}
}
int createClientKeyringEntry(int handle){
    int size = getClientKeyringSize(handle);
	if (size < 3) {
	    setClientKeyringSize(handle, size + 1);
		return size + 1;
	} else return -1;
}
int __ste_Client_Keyring_Users[3][3];
//int __ste_Client_Keyring0_User0 = 0;
//int __ste_Client_Keyring0_User1 = 0;
//int __ste_Client_Keyring0_User2 = 0;
//int __ste_Client_Keyring1_User0 = 0;
//int __ste_Client_Keyring1_User1 = 0;
//int __ste_Client_Keyring1_User2 = 0;
//int __ste_Client_Keyring2_User0 = 0;
//int __ste_Client_Keyring2_User1 = 0;
//int __ste_Client_Keyring2_User2 = 0;

int getClientKeyringUser(int handle, int index) {
	int res = 0;
	if (handle >= 0 && handle < __ste_Client_max) {
		int* keys = __ste_Client_Keyring_Users[handle];
		if (index >= 0 && index < 3) {
			res = keys[index];
		}
	}

	return res;
}


void setClientKeyringUser(int handle, int index, int value) {
	if (handle >= 0 && handle < __ste_Client_max) {
		int* keys = __ste_Client_Keyring_Users[handle];
		if (index >= 0 && index < 3) {
			keys[index] = value;
		}
	}
}

int __ste_Client_Keyring_PublicKeys[3][3];
//int __ste_Client_Keyring0_PublicKey0 = 0;
//int __ste_Client_Keyring0_PublicKey1 = 0;
//int __ste_Client_Keyring0_PublicKey2 = 0;
//int __ste_Client_Keyring1_PublicKey0 = 0;
//int __ste_Client_Keyring1_PublicKey1 = 0;
//int __ste_Client_Keyring1_PublicKey2 = 0;
//int __ste_Client_Keyring2_PublicKey0 = 0;
//int __ste_Client_Keyring2_PublicKey1 = 0;
//int __ste_Client_Keyring2_PublicKey2 = 0;

int getClientKeyringPublicKey(int handle, int index) {
	int res = 0;
	if (handle >= 0 && handle < __ste_Client_max) {
		int* keys = __ste_Client_Keyring_PublicKeys[handle];
		if (index >= 0 && index < __ste_Client_max) {
			res = keys[index];
		}
	}
	return res;
}

int findPublicKey(int handle, int userid) {
	int res = 0;
	if (handle >= 0 && handle < __ste_Client_max) {
		int* keyring = __ste_Client_Keyring_Users[handle];
		int i;
		for (i = 0; i < (__ste_Client_max - 1); i++) {  // last position is reserved to the user's own key.
			if (userid = keyring[i]) {
				res = __ste_Client_Keyring_PublicKeys[handle][userid];
			}
		}
	}
	return res;
}

void setClientKeyringPublicKey(int handle, int index, int value) {
	if (handle >= 0 && handle < __ste_Client_max) {
		int* pubkeys = __ste_Client_Keyring_PublicKeys[handle];
		if (index >= 0 && index < __ste_Client_max) {
			pubkeys[index] = value;
		}
	}
}

int __ste_client_forwardReceivers[3];
//int __ste_client_forwardReceiver0 =0;
//int __ste_client_forwardReceiver1 =0;
//int __ste_client_forwardReceiver2 =0;
//int __ste_client_forwardReceiver3 =0;
int getClientForwardReceiver(int handle) {
	int res = -1;
	if (handle >= 0 && handle < __ste_Client_max) {
		res = __ste_client_forwardReceivers[handle];
	}
	return res;
}

void setClientForwardReceiver(int handle, int value) {
	if (handle >= 0 && handle < __ste_Client_max) {
		__ste_client_forwardReceivers[handle] = value;
	}
}
int __ste_client_idCounters[5];
//int __ste_client_idCounter0 = 0;
//int __ste_client_idCounter1 = 0;
//int __ste_client_idCounter2 = 0;

int getClientId(int handle) {
	int res = 0;
	if (handle >= 0 && handle < __ste_Client_max) {
		res = __ste_client_idCounters[handle];
	}
	return res;
}
void setClientId(int handle, int value) {
	if (handle >= 0 && handle < __ste_Client_max) {
		__ste_client_idCounters[handle] = value;
	}
}
