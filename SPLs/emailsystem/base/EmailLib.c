#include <stdlib.h>
#include <string.h>
#include "EmailLib.h"

int __ste_Email_max     = 10000;
int __ste_Email_counter = 0;
int initEmail() {
	if (__ste_Email_counter < __ste_Email_max) {
		return ++__ste_Email_counter;
	} else {
		return -1;
	}
}

int __ste_email_ids[10000];
//int __ste_email_id0 = 0;
//int __ste_email_id1 = 0;
int getEmailId(int handle) {
	int res = 0;
	if (handle >= 0 && handle < __ste_Email_max) {
		res = __ste_email_ids[handle];
	}
	return res;
}
void setEmailId(int handle, int value) {
	if (handle >= 0 && handle < __ste_Email_max) {
		__ste_email_ids[handle] = value;
	}
}

int __ste_email_from[10000];
//int __ste_email_from0 = 0;
//int __ste_email_from1 = 0;
int getEmailFrom(int handle) {
	int res = 0;
	if (handle >= 0 && handle < __ste_Email_max) {
		res = __ste_email_from[handle];
	}
	return res;
}
void setEmailFrom(int handle, int value) {
	if (handle >= 0 && handle < __ste_Email_max) {
		__ste_email_from[handle] = value;
	}
}

int __ste_email_to[10000];
//int __ste_email_to0 = 0;
//int __ste_email_to1 = 0;
int getEmailTo(int handle) {
	int res = 0;
	if (handle >= 0 && handle < __ste_Email_max) {
		res = __ste_email_to[handle];
	}
	return res;
}
void setEmailTo(int handle, int value) {
	if (handle >= 0 && handle < __ste_Email_max) {
		__ste_email_to[handle] = value;
	}
}

char __ste_email_subject[10000][200];
//char* __ste_email_subject0;
//char* __ste_email_subject1;
char* getEmailSubject(int handle) {
	char* res = NULL;
	if (handle >= 0 && handle < __ste_Email_max) {
		res = __ste_email_subject[handle];
	}
	return res;
}
void setEmailSubject(int handle, char* value) {
	if (handle >= 0 && handle < __ste_Email_max) {
		strcpy(__ste_email_subject[handle], value);
	}
}

char __ste_email_body[10000][200];
//char* __ste_email_body0 = 0;
//char* __ste_email_body1 = 0;
char* getEmailBody(int handle) {
	char* res = NULL;
	if (handle >= 0 && handle < __ste_Email_max) {
		res = __ste_email_body[handle];
	}
	return res;
}
void setEmailBody(int handle, char* value) {
	if (handle >= 0 && handle < __ste_Email_max) {
		strcpy(__ste_email_body[handle], value);
	}
}

int __ste_email_isEncrypted[10000];
//int __ste_email_isEncrypted0 = 0;
//int __ste_email_isEncrypted1 = 0;
int isEncrypted(int handle) {
	int res = 0;
	if (handle >= 0 && handle < __ste_Email_max) {
		res = __ste_email_isEncrypted[handle];
	}
	return res;
}

void setEmailIsEncrypted(int handle, int value) {
	if (handle >= 0 && handle < __ste_Email_max) {
		__ste_email_isEncrypted[handle] = value;
	}
}

int __ste_email_encryptionKey[10000];
//int __ste_email_encryptionKey0 = 0;
//int __ste_email_encryptionKey1 = 0;
int getEmailEncryptionKey(int handle) {
	int res = 0;
	if (handle >= 0 && handle < __ste_Email_max) {
		res = __ste_email_encryptionKey[handle];
	}
	return res;
}

void setEmailEncryptionKey(int handle, int value) {
	if (handle >= 0 && handle < __ste_Email_max) {
		__ste_email_encryptionKey[handle] = value;
	}
}

int __ste_email_isSigned[10000];
//int __ste_email_isSigned0 = 0;
//int __ste_email_isSigned1 = 0;
int isSigned(int handle) {
	int res = 0;
	if (handle >= 0 && handle < __ste_Email_max) {
		res = __ste_email_isSigned[handle];
	}
	return res;
}
void setEmailIsSigned(int handle, int value) {
	if (handle >= 0 && handle < __ste_Email_max) {
		__ste_email_isSigned[handle] = value;
	}
}

int __ste_email_signKey[10000];
//int __ste_email_signKey0 = 0;
//int __ste_email_signKey1 = 0;
int getEmailSignKey(int handle) {
	int res = 0;
	if (handle >= 0 && handle < __ste_Email_max) {
		res = __ste_email_signKey[handle];
	}
	return res;
}

void setEmailSignKey(int handle, int value) {
	if (handle >= 0 && handle < __ste_Email_max) {
		__ste_email_signKey[handle] = value;
	}
}

int __ste_email_isSignatureVerified[10000];
//int __ste_email_isSignatureVerified0;
//int __ste_email_isSignatureVerified1;
int isVerified(int handle) {
	int res = 0;
	if (handle >= 0 && handle < __ste_Email_max) {
		res = __ste_email_isSignatureVerified[handle];
	}
	return res;
}
void setEmailIsSignatureVerified(int handle, int value) {
	if (handle >= 0 && handle < __ste_Email_max) {
		__ste_email_isSignatureVerified[handle] = value;
	}
}


// Custom
int findEmail(int from, int to, char* subject) {
	int i;
	int res = -1;
	for (i = 0; (i < 10000 && res < 0); i++) {
		int i_to     = __ste_email_to[i];
		int i_from   = __ste_email_from[i];
		char* i_subj = __ste_email_subject[i];
		int checkStr = strcmp(i_subj, subject);
		if ((from < 0 || i_from == from) && (to < 0 || i_to == to) && (subject == NULL || checkStr == 0 )) {
			res = i;
		}
	}
	return res;
}
