#include "EmailLib.h"

void printMail (EMAIL msg, int buffer);

int isReadable (EMAIL msg);

EMAIL createEmail (int from, int to, char* subj, char* body);

EMAIL cloneEmail(EMAIL msg);

// custom
EMAIL getEmail(int from, int to, char* subj);